//! @file
#include<iostream>
#include<string>
#include "psystem.h"
#include "generalmd.h"
#include "box.h"
#include "omp.h"
#include "constants.h"

/*!\mainpage
 *\image html strawhats.jpg
 *
 * Class to handle sparse as well as \f$\mbox{N}^2\f$ and vectorized matrices. Routines grouped
 * by functionality are listed in Modules. For general development aspects of this class see
 * \ref DESIGN under \b Related \b Pages. \n \n
 * \attention
 * The usage of \b deprecated \b function \b calls and adding new \b public \b members has to be
 * avoided in future development. Remaining public \b Members will be changed to \b private if
 * the routines in \b d-scfman are adapted to the new standard, so they have to be called by
 * the corresponding \b member \b access \b functions. Any backward compatibility is only \b temporary!
 *
 */

/*! \page DESIGN Design Codex of class SparsMat
 *
 * -# If new \b members, \b member \b functions or \b related \b functions are implemented, a description in
 * \b deoxygen style should be added. The function declarations should contain explicit (self-explaining) parameter names.
 * -# The \b related \b routines (e.g. sp_mult()) have to be designed for general purpose, i.e. independent of
 * the value of \b REM_SPARS.
 * -# \b Members should be \b private.
 * -# Usage of \b deprecated functions have to be avoided and - whenever - be changed in the previous code.
 * -# Filenames: \b member functions should be placed in file starting with \b SparsMat_, while \b external functions
 * like the linear algebra routines are placed in files starting with \b sp_. Files containing the \b sparse-algorithms
 * should start with spars.
 */

using namespace std;

int main(int argc, char **argv)
{
    string input_file;

    if (argc >= 1) {
        input_file = argv[1];
    }
    else
    {
        cout << "no enough command lines arrrrrrhgs" << endl;
        throw "bunnies";
    }

    System system(input_file);
    std::cout << "system made" << std::endl;
    cout << system << endl;
    cout << system.getBox().box() << endl;
    cout << system.systemParameters();
    system.getPiezo().calculateVibBasis();
    system.getGeneralMd().iterateSystem();
}
