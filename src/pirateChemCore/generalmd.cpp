#include "generalmd.h"
#include "psystem.h"
#include <cmath>
#include "integrator.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "xyzwriter.h"
#include "energywriter.h"
#include "constants.h"

using namespace std;

GeneralMd::GeneralMd(System &system) : m_system(system)
{
}


double GeneralMd::getSystemKineticEnergy()
{
    double kinEn = 0.0;
    for(unsigned int i = 0; i < m_system.size(); i++){
        kinEn += 0.5* m_system.atoms().at(i)->m() *pow(arma::norm(m_system.atoms().at(i)->v(),2),2) * amuToMe;
    }
    for(unsigned int i= 0; i < m_system.ringPolymerSize(); i++)
    {
        kinEn += 0.5* m_system.ringPolymerBeads().at(i)->m() *pow(arma::norm(m_system.ringPolymerBeads().at(i)->v(),2),2) * amuToMe;
    }
    return kinEn;
}

void GeneralMd::iterateSystem()
{
    XYZWriter* writeTrajectory = NULL;
    EnergyWriter* writeEnergy  = NULL;

    string trajectoryString = m_system.systemParameters().outStub + ".xyz";
    string energyString = m_system.systemParameters().outStub  + ".energy";

    if (m_system.systemParameters().saveTrajectory == true){
        writeTrajectory = new XYZWriter(trajectoryString);
    }

    if (m_system.systemParameters().saveEnergy == true){
        writeEnergy = new EnergyWriter(m_system, energyString);
    }

    //set temperature and kinetic energy for the system before we get going
    m_system.setTempKin();

    for(int i = 0; i< m_system.systemParameters().numSteps; i++)
    {
//        //sets temp and kinetic energy parameters in system
//        m_system.setTempKin();

        if (i % (m_system.systemParameters().printStep*100) == 0){
            cout << "Arg, I be on plank " << i << endl;
            }
        if (i % (m_system.systemParameters().printStep) == 0){

//            cout << "Arg, I be on plank " << i << endl;
////            stringstream ss; ss << "on step: " << i << endl;
////            string message; ss >> message;
////            m_system.parlay(message);

            if (m_system.systemParameters().saveTrajectory == true){
                writeTrajectory->writeFrame(m_system);
            }

            if (m_system.systemParameters().saveEnergy == true){
                writeEnergy->writeFrame();
            }
        }

        m_system.getIntegrator().integrate();

        //update current step
        m_system.systemParameters().currentStep += 1;
    }

    if (writeTrajectory !=NULL){
        delete writeTrajectory;
    }
    if (writeEnergy != NULL){
        delete writeEnergy;
    }
}

System& GeneralMd::getSystem()
{
    return m_system;
}
