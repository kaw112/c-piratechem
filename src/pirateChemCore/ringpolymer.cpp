#include "ringpolymer.h"
#include "constants.h"
#include <armadillo>
#include <cmath>

using namespace std;

RingPolymer::RingPolymer(unsigned int numBeads, double partMass, double fictMass, double partCharge, double temp, double cenX, double cenY, double cenZ)
{
    m_numBeads = numBeads;
    m_center = arma::zeros<arma::vec>(3);
    m_particleMass = partMass;
    m_fictMass = fictMass;
    m_particleCharge = partCharge;
    m_temperature = temp;
    m_center[0] = cenX;
    m_center[1] = cenY;
    m_center[2] = cenZ;
}

void RingPolymer::initialize() {
    if (m_beads.size()) {
        cerr << "You called RingPolymer::initialize more than once" << endl;
        throw -1;
    }
    for(unsigned int i = 0; i < m_numBeads; i++) {
        Atom * atom = new Atom();
        m_beads.push_back(atom);
    }
    setBeadCharges();
    setBeadMassesDefault();
    calculateForceConstant();
    buildRingPolymerPositionsBoltzmann();
}

RingPolymer::~RingPolymer()
{
    for(unsigned int i = 0; i < m_beads.size(); i++){
        if(m_beads.at(i) != NULL){
            delete m_beads[i];
        }
    }
}

void RingPolymer::setBeadCharges()
{
    double beadCharge = m_particleCharge;

    for(unsigned int i =0; i < m_beads.size(); i ++)
    {
        m_beads[i]->setQ(beadCharge);
    }
}

void RingPolymer::setBeadMassesDefault()
{
    for (unsigned int i = 0; i<m_beads.size(); i++)
    {
        m_beads[i]->setM(m_fictMass);
    }
}

std::vector<Atom*> &RingPolymer::beads()
{
    return m_beads;
}


double RingPolymer::particleMass()
{
    return m_particleMass;
}

double RingPolymer::fictMass()
{
    return m_fictMass;
}

double RingPolymer::particleCharge()
{
    return m_particleCharge;
}

void RingPolymer::calculateForceConstant()
{
    double kT = KB * m_temperature;
    //!the formula is constant = m*N^2/(beta^2 * hbar^2); but hbar is 1 in atomic units
    m_forceConstant = m_particleMass *m_numBeads* m_numBeads *amuToMe *kT *kT;
}

double RingPolymer::forceConstant()
{
    return m_forceConstant;
}

arma::vec & RingPolymer::center()
{
    return m_center;
}

const unsigned int RingPolymer::beadNumber()
{
    if (m_beads.size() != m_numBeads)
    {
        cerr << "Avast ye scurvy dog, number of beads for ring polymer be not the same as the number of atoms for the poymer" << endl;
        throw -1;
    }
    return m_numBeads;
}


void RingPolymer::newBead(std::string name, double x, double y, double z,
             double vx , double vy, double vz,
             double q, double m)
{
    Atom* atom = new Atom(name, x, y, z, vx, vy, vz, q, m);
    m_beads.push_back(atom);
}

void RingPolymer::clear()
{
    m_beads.clear();
}

void RingPolymer::newPolymer(int N)
{

}

void RingPolymer::setBeadPositions(arma::mat &pos_matrix)
{
    if(pos_matrix.n_cols != m_numBeads){
        cerr << "position matrix dimensions do not match that of number of ring polymer's beads"<< endl;
        throw -1;
    }

    for(unsigned int i = 0; i < pos_matrix.n_cols; i ++){
        arma::vec pos_vec = pos_matrix.col(i);
        m_beads[i]->setR(pos_vec);
    }
}

void RingPolymer::setBeadMasses(arma::vec& mass_vec)
{
    if(mass_vec.n_elem != beadNumber()){
        cerr << "mass vector dimensions do not match that of ring polymer's"<< endl;
        throw -1;
    }

    for(unsigned int i = 0; i < mass_vec.n_elem; i ++){
        m_beads[i]->setM(mass_vec(i));
    }
}

void RingPolymer::setBeadVelocities(arma::mat& vel_matrix)
{
    if(vel_matrix.n_cols != m_numBeads){
        cerr << "velocity matrix dimensions do not match that of ring polymer's"<< endl;
        throw -1;
    }

    for(unsigned int i = 0; i < vel_matrix.n_cols; i ++){
        arma::vec vel_vec = vel_matrix.col(i);
        m_beads[i]->setV(vel_vec);
    }
}

void RingPolymer::buildRingPolymerPositionsBoltzmann()
{
    //! This is how its gonna go down
    //! We first gonna get interbead vectors via boltzmann distribution using the force constants on the springs
    arma::mat interBeadVectors(3, m_numBeads);


    //! the first argument is center, second is standard deviation
    PirateChem::Normal random(0, sqrt(KB*m_temperature/m_forceConstant));

    //!get them interbead vectors
    for(unsigned int i = 0; i< m_numBeads; i++)
    {
        interBeadVectors(0,i) = random()*BohrToAng;
        interBeadVectors(1,i) = random()*BohrToAng;
        interBeadVectors(2,i) = random()*BohrToAng;
    }

    //! we are not done yet. we need the vectors to sum to zero
    arma::vec  interBeadSum = sum(interBeadVectors, 1);


    //! divide by bead number
    arma::vec avgInterBeadSum = interBeadSum/(m_numBeads);

    //!now we adjust the inter bead vectors
    for (unsigned int i = 0; i<m_numBeads; i++)
    {
        interBeadVectors(0,i) = interBeadVectors(0,i) - avgInterBeadSum(0);
        interBeadVectors(1,i) = interBeadVectors(1,i) - avgInterBeadSum(1);
        interBeadVectors(2,i) = interBeadVectors(2,i) - avgInterBeadSum(2);
    }

    //!now the vectors will begin and end at the same place when added together (ie. we have made a ring)
    //!so now we construct our ring polymer starting at origin

    for (unsigned int i = 1; i<m_numBeads; i++)
    {
        arma::vec newR= m_beads[i-1]->r() + interBeadVectors.col(i);
        m_beads[i]->setR(newR);
    }

    //!now to get the center
    arma::vec avgPosition = m_beads[0]->r();

    for (unsigned int i = 1; i<m_numBeads; i++)
    {
        avgPosition +=m_beads[i]->r();
    }
    avgPosition = avgPosition/(m_numBeads);

    //! now to get the correction
    arma::vec correctionVector = m_center- avgPosition;

    //! now to correct the past to save the future
    for (unsigned int i = 0; i<m_numBeads; i++)
    {
        m_beads[i]-> addR(correctionVector);
    }
}

double RingPolymer::calculateSpringEnergies()
    {
    double energy = 0.0;

    //!make sure that units are atomic
        #pragma omp parallel for
        for(unsigned int i = 0; i< m_numBeads; i++)
        {
            if(i == m_numBeads-1){
                arma::vec r = (m_beads[0]->r()-m_beads[i]->r())*angToBohr;
                double rNormSquared = arma::dot(r,r);

                energy += 0.5 * m_forceConstant *rNormSquared;

            }
            else{
            arma::vec r = (m_beads[i+1]->r()-m_beads[i]->r())*angToBohr;
            double rNormSquared = arma::dot(r,r);

            energy += 0.5 * m_forceConstant *rNormSquared;
            }
        }
        return energy;
    }

void RingPolymer::calculateForces(arma::mat &forces)
{
    if (forces.n_cols != m_numBeads)
    {
        cerr << "dimensions for the force matrix given to this ring polymer are incorrect"<<endl;
        throw -1;
    }
    //! important to make sure initial entries are all 0
    forces.zeros();


    //! take care of the cyclicness
    arma::vec dr0 = m_beads.at(0)->r()-m_beads.at(m_numBeads-1)->r();

    arma::vec force0 = m_forceConstant * dr0 * angToBohr;

    forces.col(m_numBeads-1) += force0;

    forces.col(0) += -1.0 * force0;

    //! now the rest of the inter vectors
    #pragma omp parallel for
    for(unsigned int i = 1; i<m_numBeads; i++)
    {
        arma::vec dr = m_beads.at(i)->r()-m_beads.at(i-1)->r();
        arma::vec force = m_forceConstant * dr * angToBohr;
        forces.col(i-1) += force;
        forces.col(i)   += -1.0*force;
    }
}


std::ostream& operator<<(std::ostream& stream,RingPolymer& ringPolymer)
{
    return stream << "Ring Polymer [" << ringPolymer.m_beads << "]" <<endl;
}

std::ostream& operator<<(std::ostream& stream, std::vector<RingPolymer>& vec)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        stream << vec[i];
    }
    return stream;
}
