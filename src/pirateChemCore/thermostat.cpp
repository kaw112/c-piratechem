#include "thermostat.h"
#include "psystem.h"
#include "atom.h"
#include <armadillo>
#include <cmath>
#include "constants.h"

using namespace std;

Thermostat::Thermostat(System& system) : m_system(system)
{
m_NoseHooverChain = NULL;

m_Langevin = NULL;

m_Bussi = NULL;

}

void Thermostat::setThermostat()
{

    if (m_system.systemParameters().noseHoover == true){

        if (m_Langevin != NULL){
            cerr << "Shiver me timbers! Thermostat already set" << endl;
            throw -1;
        }

        else if (m_Bussi != NULL){
            cerr << "Shiver me timbers! Thermostat already set" << endl;
            throw -1;
        }

        m_NoseHooverChain = new NoseHooverChain(m_system);
        m_system.parley("setting Thermostat to Nose Hoover Chain");
    }

    if (m_system.systemParameters().langevinTherm == true){

        if (m_Bussi != NULL){
            cerr << "Shiver me timbers! Thermostat already set" << endl;
            throw -1;
        }

        else if (m_NoseHooverChain != NULL){
            cerr << "Shiver me timbers! Thermostat already set" << endl;
            throw -1;
        }

        m_Langevin = new Langevin(m_system);
        m_system.parley("setting Thermostat to Langevin");
    }
    if (m_system.systemParameters().bussiTherm == true){

        if (m_Langevin != NULL){
            cerr << "Shiver me timbers! Thermostat already set" << endl;
            throw -1;
        }

        else if (m_NoseHooverChain != NULL){
            cerr << "Shiver me timbers! Thermostat already set" << endl;
            throw -1;
        }

        m_Bussi = new Bussi(m_system);
        m_system.parley("setting Thermostat to Bussi Velocity Rescaling");
    }
}

NoseHooverChain& Thermostat::getNose()
{
    return *m_NoseHooverChain;
}

Langevin& Thermostat::getLangevin()
{
    return *m_Langevin;
}

Bussi& Thermostat::getBussi()
{
    return *m_Bussi;
}

Thermostat::~Thermostat()
{
    if (m_NoseHooverChain != NULL)
    {
        delete m_NoseHooverChain;
    }
    if (m_Langevin != NULL)
    {
        delete m_Langevin;
    }
    if (m_Bussi != NULL)
    {
        delete m_Bussi;
    }
}

NoseHooverChain::NoseHooverChain(System& system): m_system(system)
{

    m = m_system.systemParameters().noseHooverM;
    m_xs = arma::zeros(m);
    m_vs = arma::zeros(m);
    m_qs = arma::zeros(m);
    m_gs = arma::zeros(m);

    m_kT = KB * m_system.systemParameters().temperature;
    m_L = m_system.totalSize() * 3;
    dt = femtosecToAu * m_system.systemParameters().dt;

    initQs();
}

void NoseHooverChain::initQs()
{
    double w = 1.0/.00214;
    double constant = m_kT*pow(w, 2);

    m_qs.at(0) = m_L * constant;

    for( int i = 1; i < m; i++ )
    {
        m_qs.at(i) = constant;
    }
}


void NoseHooverChain::chain()
{
    m_gs.at(m-1) = (m_qs.at(m-2) * pow(m_vs.at(m-2), 2) - m_kT)/m_qs.at(m-1);
    m_vs.at(m-1) += dt * m_gs.at(m-1)/4.0;

    for(int i = 2; i < m; i++){
        m_vs.at(m-i) *= exp(-dt/8.0* m_vs.at(m-i+1));
        m_gs.at(m-i) = (m_qs.at(m-i-1)* pow(m_vs.at(m-i-1), 2) - m_kT)/m_qs.at(m-i);
        m_vs.at(m-i) += dt/4.0 * m_gs.at(m-i);
        m_vs.at(m-i) *= exp(-dt/8.0 * m_vs.at(m-i+1));
    }

    m_vs.at(0) *= exp(-dt/8.0 *m_vs.at(1));
    double kin = 2.0*m_system.getKineticEnergy();
    m_gs.at(0) = (kin- m_L* m_kT)/m_qs.at(0);
    m_vs.at(0) += dt/4.0 * m_gs.at(0);
    m_vs.at(0) *= exp(-dt/8.0 * m_vs.at(1));

    for(int i = 0; i < m; i++){
        m_xs.at(i) += dt/2.0*m_vs.at(i);
    }

    //update actual velocities
    double constant = exp(-dt/2.0*m_vs.at(0));
    for(unsigned int i = 0; i < m_system.size(); i++){
        m_system.atoms().at(i)->scaleV(constant);
    }
    for(unsigned int i = 0; i< m_system.ringPolymerSize();i++){
        m_system.ringPolymerBeads().at(i)->scaleV(constant);
    }

    //make sure to update temperature and kinetic energy for system
    m_system.setTempKin();

    m_vs.at(0) *= exp(-dt/8.0 *m_vs.at(1));
    double kin2 = 2.0*m_system.getKineticEnergy();
    m_gs.at(0) = (kin2- m_L* m_kT)/m_qs.at(0);
    m_vs.at(0) += dt/4.0 * m_gs.at(0);
    m_vs.at(0) *= exp(-dt/8.0 * m_vs.at(1));

    for(int i = 1; i < m-1; i++){
        m_vs.at(i) *= exp(-dt/8.0* m_vs.at(i+1));
        m_gs.at(i) = (m_qs.at(i-1)* pow(m_vs.at(i-1), 2) - m_kT)/m_qs.at(i);
        m_vs.at(i) += dt/4.0 * m_gs.at(i);
        m_vs.at(i) *= exp(-dt/8.0 * m_vs.at(i+1));

    }

    m_gs.at(m-1) = (m_qs.at(m-2) * pow(m_vs.at(m-2), 2) - m_kT)/m_qs.at(m-1);
    m_vs.at(m-1) += dt * m_gs.at(m-1)/4.0;
}

Bussi::Bussi(System &system): m_system(system)
{
    dt = femtosecToAu * m_system.systemParameters().dt;

    tau = femtosecToAu * m_system.systemParameters().bussiTau;

    //! target energy is 3/2 NkT, assume 3D
    kinTarget = 1.5 * KB * m_system.systemParameters().temperature * m_system.totalSize();

    expTOverTau = exp(-dt/tau);

    expTOver2Tau = exp(-dt/(2*tau));

    m_randNums = arma::vec(m_system.totalSize() * 3);

    m_rand.setParameters(0, 1);

}

Bussi::~Bussi()
{
}

void Bussi::letsGetBussi()
{

    double kin = m_system.getKineticEnergy();

    #pragma omp for
    for(unsigned int i = 0; i< 3* m_system.totalSize(); i++)
    {
        m_randNums[i] = m_rand();
    }
    double R1 = m_randNums[0];
    double sumRSquared =  arma::dot(m_randNums, m_randNums);

    double alpha_squared = expTOverTau + kinTarget/(3 * m_system.totalSize() * kin) *(1-expTOverTau)*sumRSquared
            + 2.0* expTOver2Tau*sqrt(kinTarget/(3 * m_system.totalSize() * kin) *(1-expTOverTau)) *R1;

    double scaleFactor = sqrt(alpha_squared);

    #pragma omp for
    for(unsigned int i = 0; i < m_system.size(); i++){
        m_system.atoms().at(i)->scaleV(scaleFactor);
    }
    #pragma omp for
    for(unsigned int j = 0; j< m_system.ringPolymerSize(); j++){
        m_system.ringPolymerBeads().at(j)->scaleV(scaleFactor);
    }
}

Langevin::Langevin(System &system): m_system(system)
{

}
