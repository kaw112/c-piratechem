#include "xyzwriter.h"
#include "molecule.h"
#include "psystem.h"

using namespace std;

XYZWriter::XYZWriter(const std::string &filename)
{
    m_handle.open(filename.c_str(), ios::app);
    if(! m_handle)
    {
        throw std::ios_base::failure("Error opening output file");
    }
}

void XYZWriter::writeFrame(Molecule& molecule)
{
    m_handle << molecule.size() << endl << endl;

    for(unsigned int i = 0; i < molecule.size(); i++){
        m_handle << molecule.atoms().at(i)->name() << " "
                 << molecule.atoms().at(i)->r()[0] << " "
                 << molecule.atoms().at(i)->r()[1] << " "
                 << molecule.atoms().at(i)->r()[2] << '\n';
    }
    //m_handle.flush();
}

void XYZWriter::writeFrame(System &system)
{
    m_handle << system.totalSize() << endl << "step " << system.systemParameters().currentStep << " "<<
                "time " << system.systemParameters().dt * system.systemParameters().currentStep<<endl;

    for(unsigned int i = 0; i < system.size(); i++){
        m_handle << system.atoms().at(i)->name() << " "
                 << system.atoms().at(i)->r()[0] << " "
                 << system.atoms().at(i)->r()[1] << " "
                 << system.atoms().at(i)->r()[2] << " "
                 << system.atoms().at(i)->v()[0] << " "
                 << system.atoms().at(i)->v()[1] << " "
                 << system.atoms().at(i)->v()[2] << " "
                 << system.atoms().at(i)->m() << "\n";
    }
    for(unsigned int i = 0; i < system.ringPolymerSize(); i++){
        m_handle << "C "
                 << system.ringPolymerBeads().at(i)->r()[0] << " "
                 << system.ringPolymerBeads().at(i)->r()[1] << " "
                 << system.ringPolymerBeads().at(i)->r()[2] << " "
                 << system.ringPolymerBeads().at(i)->v()[0] << " "
                 << system.ringPolymerBeads().at(i)->v()[1] << " "
                 << system.ringPolymerBeads().at(i)->v()[2] << " "
                 << system.ringPolymerBeads().at(i)->m() << "\n";
    }
    //m_handle.flush();
}
