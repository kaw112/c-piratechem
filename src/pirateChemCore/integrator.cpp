#include "integrator.h"
#include "psystem.h"
#include <iostream>
#include "potentials.h"
#include "box.h"
#include "thermostat.h"
#include "constants.h"

using namespace std;

Integrator::Integrator(System& system) : m_system(system)
{
    //m_fij = arma::cube(m_system.size(), m_system.size(), 3);

    m_fs = arma::mat(3, m_system.size());

    m_fsRing = arma::mat(3, m_system.ringPolymerSize());

//    m_ringPartMass = arma::vec(m_system.ringPolymerSize());

//    int counter = 0;
//    for (unsigned int i = 0; i<m_system.ringPolymers().size(); i++)
//    {
//        for(unsigned int j= 0; j<m_system.ringPolymers().at(i)->beadNumber(); j++)
//        {
//            m_ringPartMass[counter] = m_system.ringPolymers().at(i)->particleMass();
//            counter += 1;
//        }
//    }

    m_ljCalc = new LjCalc(m_system);

    m_springCalc = new SpringCalc(m_system);

    m_coulombCalc = new CoulombCalc(m_system);

    m_coulombInterbead = new CoulombInterbead(m_system);

    dt = femtosecToAu * m_system.systemParameters().dt;

    m_coulombEnergy = 0.0;

    m_springEnergy = 0.0;

    m_ljEnergy =  0.0;

    m_ringPolymerSpringEnergies = 0.0;

    m_ringPolymerInterbeadEnergies = 0.0;

}

Integrator::~Integrator()
{
    if (m_ljCalc != NULL)
    {
        delete m_ljCalc;
    }
    if (m_springCalc != NULL)
    {
        delete m_springCalc;
    }
    if (m_coulombCalc != NULL)
    {
        delete m_coulombCalc;
    }
    if (m_coulombInterbead != NULL)
    {
        delete m_coulombInterbead;
    }
}

void Integrator::calculateForces()
{
    //m_fij.zeros();
    //!set the force matrices to zero
    m_fs.zeros();

    m_fsRing.zeros();

    m_coulombEnergy = 0;

    m_springEnergy = 0;

    m_ljEnergy = 0;

    m_ringPolymerSpringEnergies = 0.0;

    m_ringPolymerInterbeadEnergies = 0.0;

    if (m_system.systemParameters().coulomb == true){
        forcesCoulomb();
    }

    if (m_system.systemParameters().lj == true) {
        forcesLj();
    }

    if (m_system.systemParameters().spring == true) {
        forcesSpring();
    }

    if (m_system.systemParameters().rpmd == true){
        intraRpmdForces();
        interRpmdForces();
    }
    if (m_system.systemParameters().piezo == true){
        forcesPiezo();
    }
//    // we need to add our interbead forces to the to the ringbolimer
//    int counter = 0;

//    for(unsigned int i = 0; i< m_system.ringPolymers().size(); i++){
//        arma::mat forces(3,m_system.ringPolymers().at(i)->beadNumber());

//        m_system.ringPolymers().at(i)->calculateForces(forces);
//        m_fsRing.submat(0,counter, 2, counter + m_system.ringPolymers().at(i)->beadNumber()- 1) +=forces;
//        counter += m_system.ringPolymers().at(i)->beadNumber();
//        m_ringPolymerSpringEnergies += m_system.ringPolymers().at(i)->calculateSpringEnergies();
//        }

//    //start ring polymer part

////    for(unsigned int k= 0; k<3; k++){
////        //#pragma omp parallel for
////            for(unsigned int j= 0; j < m_system.size(); j++){
////                for(unsigned int i = 0; i < m_system.size(); i++){
////                    m_fs(k, j) += m_fij(i,j,k);
////                }
////            }
////    }
}

void Integrator::intraRpmdForces()
{

    #pragma omp parallel for
    for(unsigned int i = 0; i< m_system.ringPolymers().size(); i++){
        //! we need a starting counter to keep track of our submatrix
        int counter = 0;
        for (unsigned int j = 0; j<i; j++)
        {
            counter += m_system.ringPolymers().at(j)->beadNumber();
        }
        arma::mat forces(3,m_system.ringPolymers().at(i)->beadNumber());
        forces.zeros();

        m_system.ringPolymers().at(i)->calculateForces(forces);
        m_fsRing.submat(0,counter, 2, counter + m_system.ringPolymers().at(i)->beadNumber()- 1) +=forces;

        m_ringPolymerSpringEnergies += m_system.ringPolymers().at(i)->calculateSpringEnergies();
        }
}

void Integrator::interRpmdForces(){

    //! we must make sure to consider all ring polymer pairs once and only once

    for (unsigned int i = 0; i<m_system.ringPolymers().size();i++){
        //! we need to keep track of the bead index
        int beadStartIndexi = 0;
        for (unsigned int l = 0; l <i; l++){
            beadStartIndexi += m_system.ringPolymers().at(l)->beadNumber();
        }

        //! we need to keep track of the bead index
        int beadStartIndexj = beadStartIndexi + m_system.ringPolymers().at(i)->beadNumber();

        for(unsigned int j= i+1; j<m_system.ringPolymers().size(); j++){

            //!first we need to assess whether our ring polymers have the same number of beads or some multiple number
            if(m_system.ringPolymers().at(i)->beadNumber() == m_system.ringPolymers().at(j)->beadNumber())
            {
                #pragma omp parallel for
                for(unsigned int k = 0 ; k < m_system.ringPolymers().at(i)->beadNumber(); k++){

                    int beadIndexi = beadStartIndexi + k;
                    int beadIndexj = beadStartIndexj + k;
                    //! now we get to the meat of the matter
                    arma::vec force = arma::zeros<arma::vec>(3);
                    m_coulombInterbead->forces(beadIndexi, beadIndexj, force);

                    m_fsRing.col(beadIndexj) += force;
                    m_fsRing.col(beadIndexi) += -force;

                    m_ringPolymerInterbeadEnergies += m_coulombInterbead->energy(beadIndexi, beadIndexj);

                }

            }
            else if (m_system.ringPolymers().at(i)->beadNumber() < m_system.ringPolymers().at(j)->beadNumber())
            {
                if(m_system.ringPolymers().at(j)->beadNumber()%m_system.ringPolymers().at(i)->beadNumber() != 0){
                    cerr << "Ahoy Matey! Ring Polymer " << i << " and Ring Polymer " << j << " be not multiples of one another."<<endl;
                    throw "baby seals";
                }
                else{int ratio = m_system.ringPolymers().at(j)->beadNumber()/m_system.ringPolymers().at(i)->beadNumber();}
            }
            else if(m_system.ringPolymers().at(i)->beadNumber() > m_system.ringPolymers().at(j)->beadNumber())
            {
                if(m_system.ringPolymers().at(i)->beadNumber()%m_system.ringPolymers().at(j)->beadNumber() != 0){
                    cerr << "Ahoy Matey! Ring Polymer " << i << " and Ring Polymer " << j << " be not multiples of one another."<<endl;
                    throw "baby seals";
                }
                else{int ratio = m_system.ringPolymers().at(i)->beadNumber()/m_system.ringPolymers().at(j)->beadNumber();}
            }

        }


    }
}

void Integrator::forcesLj()
{
    if(m_system.systemConnectivity().conLJ().size() == 0){
        cerr << "Avast ye scurvy dog, no connectivity vector for LJs was created Ben was here." << endl;
        throw -1;
    }
    #pragma omp parallel for
        for(unsigned int k = 0; k < m_system.systemConnectivity().conLJ().size(); k++){
            arma::vec force = arma::zeros<arma::vec>(3);
            int i = m_system.systemConnectivity().conLJ().at(k).atom1;
            int j = m_system.systemConnectivity().conLJ().at(k).atom2;
            m_ljCalc->forces(k, force);

            m_fs.col(j) += force;
            m_fs.col(i) += -force;

            m_ljEnergy += m_ljCalc->energy(k);
        }

}

void Integrator::forcesCoulomb()
{
    if(m_system.systemConnectivity().conCoulomb().size() == 0){
        cerr << "Avast ye scurvy dog, no connectivity vector for Coulombs was created - shit balls ass" << endl;
        throw -1;
    }

    #pragma omp parallel for
        for(unsigned int k = 0; k < m_system.systemConnectivity().conCoulomb().size(); k++){
            arma::vec force = arma::zeros<arma::vec>(3);
            int i = m_system.systemConnectivity().conCoulomb().at(k).atom1;
            int j = m_system.systemConnectivity().conCoulomb().at(k).atom2;
            m_coulombCalc->forces(k, force);


            m_fs.col(j) += force;
            m_fs.col(i) += -force;



            m_coulombEnergy += m_coulombCalc->energy(k);
        }
}

void Integrator::forcesSpring()
{
    if(m_system.systemConnectivity().conSpring().size() == 0){
        cerr << "Avast ye scurvy dog, no connectivity vector for Springs was created" << endl;
        throw -1;
    }

    #pragma omp parallel for
        for(unsigned int k = 0; k < m_system.systemConnectivity().conSpring().size(); k++){
            arma::vec force = arma::zeros<arma::vec>(3);
            int i = m_system.systemConnectivity().conSpring().at(k).atom1;
            int j = m_system.systemConnectivity().conSpring().at(k).atom2;
            m_springCalc->forces(k, force);


            m_fs.col(j) += force;
            m_fs.col(i) += -force;



            m_springEnergy += m_springCalc->energy(k);
        }
}

void Integrator::forcesPiezo()
{
  arma::mat piezoForces = m_system.getPiezo().piezoForces();

  m_fs += piezoForces;
}
double Integrator::getLjEnergy()
{
    return m_ljEnergy;
}

double Integrator::getCoulombEnergy()
{
    return m_coulombEnergy;
}

double Integrator::getSpringEnergy()
{
    return m_springEnergy;
}

double Integrator::getRingPolymerSpringEnergies()
{
    return m_ringPolymerSpringEnergies;
}

double Integrator::getRingPolymerInterbeadEnergies()
{
    return m_ringPolymerInterbeadEnergies;
}

double Integrator::getTotalEnergy()
{
    return m_ljEnergy + m_coulombEnergy + m_springEnergy + m_ringPolymerSpringEnergies + m_ringPolymerInterbeadEnergies;
}



VelocityVerlet::VelocityVerlet(System& system) : Integrator(system)
{

}

void VelocityVerlet::integrate()
{
    //! forces will be calculated again later in verlet algorithm, no need to calculate again
    if(m_system.systemParameters().currentStep == 0){
        calculateForces();
    }

    //! if nosehoover thermostat is turned on
    if(m_system.systemParameters().noseHoover == true){
        m_system.getThermostat().getNose().chain();
    }

    //! if bussi thermostat is turned on
    if(m_system.systemParameters().bussiTherm == true)
    {
        m_system.getThermostat().getBussi().letsGetBussi();
    }

    updatePositions();
    updateVelocities();
    //always set temperature and kinetic energy for system right after updating velocities
    m_system.setTempKin();

    //! if nosehoover thermostat is turned on
    if(m_system.systemParameters().noseHoover == true){
        m_system.getThermostat().getNose().chain();
    }


}

void VelocityVerlet::updatePositions()
{
    #pragma omp parallel for
    for(unsigned int i = 0; i < m_system.size(); i++){

        arma::vec rAdd = BohrToAng*(m_system.atoms().at(i)->v()*dt +
                0.5/(m_system.atoms().at(i)->m()*amuToMe) * m_fs.col(i) *dt * dt);
        //cout << "dt is" << dt << endl;
        //cout << "rAdd is" << rAdd <<endl;
        //cout << "fUck dA Police" << endl;
        //cout << "v is " << m_system.atoms().at(i)->v()*dt << endl;
        //cout << "f is" << m_fs.col(i);


        //!must convert masses to atomic units first
        m_system.atoms().at(i)->addR(rAdd);


        //! check to see if needs to be put back in the box for pbc
        if (m_system.systemParameters().pbc == true){

            arma::vec rNew = m_system.atoms().at(i)->r();
            //cout << "rNew is " << rNew << endl;


            arma::vec newRnew = m_system.getBox().fix_pbc(rNew);

            //cout << "new Rnew is " << newRnew << endl;

            m_system.atoms().at(i)->setR(newRnew);
        }

    }
    // do this for ring polymers now and add pbc later
    #pragma omp parallel for
    for(unsigned int i = 0; i < m_system.ringPolymerSize(); i++){
        arma::vec rAdd = BohrToAng*(m_system.ringPolymerBeads().at(i)->v()*dt +
                0.5/(m_system.ringPolymerBeads().at(i)->m()*amuToMe) * m_fsRing.col(i) *dt *dt);

        m_system.ringPolymerBeads().at(i)->addR(rAdd);
    }
}

void VelocityVerlet::updateVelocities()
{
    #pragma omp parallel for
    for(unsigned int i = 0; i < m_system.size(); i++){

        arma::vec vAdd =.5/(m_system.atoms().at(i)->m()*amuToMe)*m_fs.col(i)* dt;

        m_system.atoms().at(i)->addV(vAdd);
    }

    #pragma omp parallel for
    for(unsigned int i = 0; i < m_system.ringPolymerSize(); i++){

        arma::vec vAdd =.5/(m_system.ringPolymerBeads().at(i)->m()*amuToMe)*m_fsRing.col(i)* dt;

        m_system.ringPolymerBeads().at(i)->addV(vAdd);
    }

        calculateForces();

    #pragma omp parallel for
    for(unsigned int i = 0; i < m_system.size(); i++){
        arma::vec vAdd2 =.5/(m_system.atoms().at(i)->m()*amuToMe)*m_fs.col(i)* dt;

        m_system.atoms().at(i)->addV(vAdd2);
    }

    #pragma omp parallel for
    for(unsigned int i = 0; i < m_system.ringPolymerSize(); i++){
        arma::vec vAdd2 =.5/(m_system.ringPolymerBeads().at(i)->m()*amuToMe)*m_fsRing.col(i)* dt;

        m_system.ringPolymerBeads().at(i)->addV(vAdd2);
    }
}
