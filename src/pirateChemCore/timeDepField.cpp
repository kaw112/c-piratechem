#include "timeDepField.h"
#include "psystem.h"
#include <boost/lexical_cast.hpp>
#include <armadillo>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/utility.hpp>
#include "waveForm.h"

TimeDepField::TimeDepField(System &system) : m_system(system)
{


}
TimeDepField::~TimeDepField()
{
  for(unsigned int i = 0; i<m_sineWaves.size(); i++){
    if (m_sineWaves.at(i) != NULL){
        delete m_sineWaves[i];
    }
  }

}

void TimeDepField::addSineWave(double phase, double wvX, double wvY, double wvZ, double frequency)
{
    SineWave* sineWave = new SineWave(phase, wvX, wvY, wvZ, frequency);
    m_sineWaves.push_back(sineWave);
}

arma::vec TimeDepField::calcTotalSineField(double time)
{
  arma::vec totalSineField = arma::zeros<arma::vec>(3);
  for (unsigned int i = 0; i<m_sineWaves.size(); i++){
    totalSineField += m_sineWaves.at(i)->getField(time);
  }
  return totalSineField;
}

std::vector<SineWave*>& TimeDepField::getSineWaves()
{
  return m_sineWaves;
}
