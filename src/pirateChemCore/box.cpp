#include "box.h"
#include<cmath>
//#include<iostream>

using namespace std;

Box::Box(arma::mat& matrix)
{
    m_box = matrix;

    m_a = matrix.col(0);
    m_b = matrix.col(1);
    m_c = matrix.col(2);
}

Box::~Box()
{
}

arma::mat& Box::box()
{
    return m_box;
}


arma::vec& Box::a()
{
    return m_a;
}

arma::vec& Box::b()
{
    return m_b;
}

arma::vec& Box::c()
{
    return m_c;
}

void Box::printVolume()
{
    cout << arma::cross(m_a, m_b) << endl;
    cout << abs(arma::dot(m_c,arma::cross(m_a, m_b))) << endl;
}

arma::vec Box::fix_pbc(arma::vec& vector)
{
    return vector;
}

arma::vec Box::dr(const arma::vec &ri, const arma::vec &rj)
{
    return rj - ri;
}

void Box::setA(arma::vec& vector)
{
    m_a = vector;
    m_box.col(0) = vector;
}

void Box::setB(arma::vec& vector)
{
    m_b = vector;
    m_box.col(1) = vector;
}

void Box::setC(arma::vec& vector)
{
    m_c = vector;
    m_box.col(2)= vector;
}



void Box::recalculate()
{

}

BoxRectangular::BoxRectangular(arma::mat& matrix) : Box(matrix)
{
    makeDiagBox();
}

void BoxRectangular::makeDiagBox()
{
    for(int i = 0; i<3; i++){
        if(m_a(i) != 0.0){
            m_box.col(i) = m_a;
        }
    }

    for(int i = 0; i<3; i++){
        if(m_b(i) != 0.0){
            m_box.col(i) = m_b;
        }
    }

    for(int i = 0; i<3; i++){
        if(m_c(i) != 0.0){
            m_box.col(i) = m_c;
        }
    }

    m_a = m_box.col(0);
    m_b = m_box.col(1);
    m_c = m_box.col(2);
}

arma::vec BoxRectangular::fix_pbc(arma::vec& posvec)
{
    arma::vec diag = arma::diagvec(m_box);
    double ai;
    double bi;
    double ci;
    double a_f = posvec(0)/diag(0);
    double b_f = posvec(1)/diag(1);
    double c_f = posvec(2)/diag(2);

    arma::vec fvec(3);
    fvec(0) = modf(a_f, &ai);
    fvec(1) = modf(b_f, &bi);
    fvec(2) = modf(c_f, &ci);

    for(int i = 0; i<3; i++){
        if(fvec(i)<0.0){
            fvec(i) ++;
        }
    }
    return fvec % diag;
}

arma::vec BoxRectangular::dr(const arma::vec &ri, const arma::vec &rj)
{
    arma::vec diffvec = rj - ri;

    double dx = diffvec(0);
    double dy = diffvec(1);
    double dz = diffvec(2);

    double xlen = m_box(0,0);
    double ylen = m_box(1,1);
    double zlen = m_box(2,2);

    if(dx < -xlen * 0.5){
        dx = dx + xlen;
    }

    if(dx >= xlen * 0.5){
        dx = dx - xlen;
    }

    if(dy < -ylen * 0.5){
        dy = dy + ylen;
    }

    if(dy >= ylen * 0.5){
        dy = dy - ylen;
    }

    if(dz < -zlen * 0.5){
        dz = dz + zlen;
    }

    if(dz >= zlen * 0.5){
        dz = dz - zlen;
    }

    diffvec(0) = dx;
    diffvec(1) = dy;
    diffvec(2) = dz;
    return diffvec;
}

BoxTriclinic::BoxTriclinic(arma::mat& matrix) : Box(matrix)
{
    m_inverse = m_box.i();
}

arma::mat BoxTriclinic::inverse()
{
    return m_inverse;
}


arma::vec BoxTriclinic::fix_pbc(arma::vec& vector)
{
    arma::vec scaling_vec = m_inverse * vector;
    double ai;
    double bi;
    double ci;
    double a_f = scaling_vec(0);
    double b_f = scaling_vec(1);
    double c_f = scaling_vec(2);

    arma::vec new_scaling_vec(3);

    new_scaling_vec(0) = modf(a_f, &ai);
    new_scaling_vec(1) = modf(b_f, &bi);
    new_scaling_vec(2) = modf(c_f, &ci);

    for(int i = 0; i<3; i++){
        if (new_scaling_vec(i)< 0.0){
        new_scaling_vec(i) += 1.0;
        }
    }

    return m_box * new_scaling_vec;
}

arma::vec BoxTriclinic::dr(arma::vec& ri, arma::vec& rj)
{
    arma::vec diffvec = rj - ri;

    arma::vec scaling_vec = m_inverse*diffvec;

    for(int i = 0; i<3; i++){
        if (scaling_vec(i)< -.5){
            scaling_vec(i) += 1.0;
        }
        else if (scaling_vec(i)> .5){
            scaling_vec(i) -= 1.0;
        }
    }

    return m_box* scaling_vec;
}
