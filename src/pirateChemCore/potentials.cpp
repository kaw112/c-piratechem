#include "potentials.h"
#include "psystem.h"
#include "armadillo"
#include "box.h"
#include "constants.h"
#include <iostream>

using namespace std;

LjCalc::LjCalc(System &system) : m_system(system)
{

}

void LjCalc::forces(int i, arma::vec& force)
{
    int j = m_system.systemConnectivity().conLJ().at(i).atom1;
    int k = m_system.systemConnectivity().conLJ().at(i).atom2;
    double s2 = m_system.systemConnectivity().conLJ().at(i).s;
    s2 = s2 * s2;

    arma::vec dr = m_system.getBox().dr(m_system.atoms().at(j)->r(), m_system.atoms().at(k)->r());

    double r2 = arma::dot(dr, dr);

    if (r2 < m_system.systemParameters().lj_cutoff) {
        r2 = 1.0 / r2; // inverse r2
        s2 = s2 * s2 * s2;
        force =  24.0 * m_system.systemConnectivity().conLJ().at(i).e * s2 * r2 * r2 * r2 * r2 * (
            2.0 * s2 * r2 * r2 * r2 - 1.0) * dr * BohrToAng;\
    }
}

double LjCalc::energy(int i)
{
    int j = m_system.systemConnectivity().conLJ().at(i).atom1;
    int k = m_system.systemConnectivity().conLJ().at(i).atom2;
    double s = m_system.systemConnectivity().conLJ().at(i).s;

    arma::vec dr = m_system.getBox().dr(m_system.atoms().at(j)->r(), m_system.atoms().at(k)->r());

    double r2 = arma::dot(dr, dr);

    if (r2 < m_system.systemParameters().lj_cutoff) {
        r2 = 1.0/r2; // inverse r2
        r2 = s *s* r2;
        r2 = r2 * r2 * r2;

        return 4.0 * m_system.systemConnectivity().conLJ().at(i).e * r2 * ( r2 - 1.0);
    }
    return 0.0;
}

SpringCalc::SpringCalc(System &system) : m_system(system)
{

}

void SpringCalc::forces(int i, arma::vec & force)
{
    int j = m_system.systemConnectivity().conSpring().at(i).atom1;
    int k = m_system.systemConnectivity().conSpring().at(i).atom2;
    double kconst = m_system.systemConnectivity().conSpring().at(i).mk;
    double r0 = m_system.systemConnectivity().conSpring().at(i).mr0;

    arma::vec dr = m_system.getBox().dr(m_system.atoms().at(j)->r(), m_system.atoms().at(k)->r());

    double r = arma::norm(dr, 2);

    if (r-r0 != 0.0 ){
            force = -1.0 * kconst * (r-r0) * dr/r *BohrToAng;
    }



}

double SpringCalc::energy(int i)
{
    int j = m_system.systemConnectivity().conSpring().at(i).atom1;
    int k = m_system.systemConnectivity().conSpring().at(i).atom2;
    double kconst = m_system.systemConnectivity().conSpring().at(i).mk;
    double r0 = m_system.systemConnectivity().conSpring().at(i).mr0;

    arma::vec dr = m_system.getBox().dr(m_system.atoms().at(j)->r(), m_system.atoms().at(k)->r());

    double r = arma::norm(dr, 2);

    return 0.5 * kconst * (r-r0) * (r-r0);
}

CoulombCalc::CoulombCalc(System &system) : m_system(system)
{

}

void CoulombCalc::forces(int i, arma::vec &force)
{
    int j = m_system.systemConnectivity().conCoulomb().at(i).atom1;
    int k = m_system.systemConnectivity().conCoulomb().at(i).atom2;
    double q1 = m_system.atoms().at(j)->q();
    double q2 = m_system.atoms().at(k)->q();

    arma::vec dr = m_system.getBox().dr(m_system.atoms().at(j)->r(), m_system.atoms().at(k)->r());

    double ri = 1.0/arma::norm(dr, 2);

    //! using atomic units ensures prefactor goes to 1
    force = q1 * q2* ri*ri*ri * dr * BohrToAng * BohrToAng;

}

double CoulombCalc::energy(int i)
{
    int j = m_system.systemConnectivity().conCoulomb().at(i).atom1;
    int k = m_system.systemConnectivity().conCoulomb().at(i).atom2;
    double q1 = m_system.atoms().at(j)->q();
    double q2 = m_system.atoms().at(k)->q();

    arma::vec dr = m_system.getBox().dr(m_system.atoms().at(j)->r(), m_system.atoms().at(k)->r());

    double ri = 1.0/arma::norm(dr, 2);

    return q1 * q2* ri * BohrToAng;
}

CoulombInterbead::CoulombInterbead(System &system): m_system(system)
{

}

void CoulombInterbead::forces(int i, int j, arma::vec &force)
{

    arma::vec dr = m_system.getBox().dr(m_system.ringPolymerBeads().at(i)->r(), m_system.ringPolymerBeads().at(j)->r());
     double ri = 1.0/arma::norm(dr, 2);

   //! using atomic units ensures prefactor goes to 1
     const double thr = 5.0e1;

   if (ri<thr)
   {
        force = m_system.ringPolymerBeads().at(i)->q() * m_system.ringPolymerBeads().at(j)->q()* ri*ri*ri * dr * BohrToAng * BohrToAng;
    }
   else
   {
       force = m_system.ringPolymerBeads().at(i)->q() * m_system.ringPolymerBeads().at(j)->q()* thr *thr*thr * dr * BohrToAng * BohrToAng;
   }
}

double CoulombInterbead::energy(int i, int j)
{

    arma::vec dr = m_system.getBox().dr(m_system.ringPolymerBeads().at(i)->r(), m_system.ringPolymerBeads().at(j)->r());
    double ri = 1.0/arma::norm(dr, 2);

    return m_system.ringPolymerBeads().at(i)->q() * m_system.ringPolymerBeads().at(j)->q()* ri  * BohrToAng;
}
