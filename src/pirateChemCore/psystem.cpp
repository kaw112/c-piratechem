#include "psystem.h"
#include "molecule.h"
#include "xyzreader.h"
#include "inputparser.h"
#include "random.h"
#include "integrator.h"
#include "generalmd.h"
#include "thermostat.h"
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <armadillo>
#include "box.h"
#include "constants.h"

using namespace std;

System::System(const string &inputFile) : m_box(NULL), m_size(0), m_ringPolymerSize(0)
{
    initialize(inputFile);
    m_inputFile = inputFile;

}

void System::initialize(const string &inputFile)
{


    //! set default box
    arma::mat matrix("1 0 0; 0 1 0; 0 0 1");
    setBox(matrix);

    //! set connectivity (must be made before input parser)
    m_systemConnectivity = new Connectivity(*this);

    //! set generalmd
    m_systemGeneralMd = new GeneralMd(*this);
    //! set time Dep Field object; necessary to load in wave form objects if necessary
    setTimeDepField();

    parley("parser start");
    //! parse input file
    InputParser parser(*this);
    parser.parse(inputFile);
    parley("parser finish");

    //! seed the random number generator
    PirateChem::Random rand;
    rand.seed(m_systemParameters.randomseed);
    parley("seeding random number generator");

    if(m_systemParameters.uniformSystem == true){
        setUniformSystem();
    }

    if(m_systemParameters.randomPositions == true){
        setRandomPositions();
    }
    if(m_systemParameters.uniformPositions == true){
        setUniformPositions();
    }
    if(m_systemParameters.piezo == true){
        setPiezo();
    }

    //! set up connectivity if standard is required via parameters
    setCoulombConnectivity();
    setSpringConnectivity();
    setLJConnectivity();

    //! set up initial maxwell distribution velocities if so desired
    setMaxwellVelocities();

    //! just to be safe
    m_systemIntegrator = NULL;
    m_systemThermostat = new Thermostat(*this);

    //! set integrator type
    setIntegrator();

    //! set thermostat type
    setThermostat();
}


double System::time()
{
  double time = m_systemParameters.dt*m_systemParameters.currentStep;
  return time;
}

void System::setCoulombConnectivity()
{
    if (m_systemParameters.coulombConStandard == true) {
        parley("overiding coulomb connectivity to standard");
        m_systemConnectivity->standardConCoulomb();
    }
}

void System::setSpringConnectivity()
{
    if (m_systemParameters.springConStandard == true) {
        parley("overiding spring connectivity to standard");
        m_systemConnectivity->standardConSpring();
    }
}

void System::setLJConnectivity()
{
    if (m_systemParameters.ljConStandard == true) {
        parley("overiding lj connectivity to standard");
        m_systemConnectivity->standardConLJ();
    }
}

void System::setMaxwellVelocities()
{
    if (m_systemParameters.maxwellVelocities == true){
        for(unsigned int i = 0 ; i < size(); i ++){
            PirateChem::Normal random(0,sqrt(m_systemParameters.k * m_systemParameters.temperature/(m_atoms.at(i)->m() * amuToMe)));
            arma::vec vel(3);
            vel[0] = random();
            vel[1] = random();
            vel[2] = random();
            m_atoms.at(i)->setV(vel);
        }
        //! for rpmd we need to add a part
        if (m_systemParameters.rpmd == true){
            for(unsigned int i = 0; i<ringPolymerSize(); i++){
                PirateChem::Normal random(0,sqrt(m_systemParameters.k * m_systemParameters.temperature/(m_ringPolymerBeads.at(i)->m() * amuToMe)));
                arma::vec vel(3);
                vel(0) = random();
                vel(1) = random();
                vel(2) = random();
                m_ringPolymerBeads.at(i)->setV(vel);
            }
        }
        destroyComVel();
        parley("the center of mass velocity has been destroyed");
    }
}

void System::setRandomPositions()
{
    PirateChem::UniformReal random(0,1);
    for(unsigned int i = 0; i < size(); i++){

        arma::vec v1 = m_box ->a() * random();
        arma::vec v2 = m_box ->b() * random();
        arma::vec v3 = m_box ->c() * random();

        arma::vec v = v1 + v2 + v3;

        atoms().at(i)->setR(v);
    }
}

void System::setUniformPositions()
{
    unsigned int numberPerSide = ceil(pow(size(), 0.3333333));

    double frac = 1.0/numberPerSide;

    for(unsigned int i = 0; i< numberPerSide; i++){
            for(unsigned int j = 0; j< numberPerSide; j++){
                for(unsigned int k = 0; k< numberPerSide; k++){
                    arma::vec v1 = m_box ->a() * frac * i;
                    arma::vec v2 = m_box ->b() * frac * j;
                    arma::vec v3 = m_box ->c() * frac * k;
                    arma::vec v = v1 + v2 + v3;
                    unsigned int counter = i * numberPerSide * numberPerSide + j* numberPerSide + k;
                    if (counter< size()){
                        atoms().at(counter)->setR(v);
                    }
                }
            }
    }

}

void System::setUniformSystem()
{
    Molecule& molecule = newMolecule(m_systemParameters.uniformSystemSize);
    for(unsigned int i = 0; i < molecule.size(); i ++){
        molecule.atoms().at(i)->setM(m_systemParameters.uniformMass);
        molecule.atoms().at(i)->setName(m_systemParameters.uniformName);
    }
}

arma::vec System::netMom()
{
    arma::vec netMomentum("0 0 0");
    for(unsigned int i = 0; i < size(); i++){
        netMomentum += m_atoms.at(i)->m() * m_atoms.at(i)->v();
    }
    if (m_systemParameters.rpmd == true)
    {
        for(unsigned int i = 0; i< ringPolymerSize(); i++)
        {
            netMomentum += m_ringPolymerBeads.at(i)->m() * m_ringPolymerBeads.at(i)->v();
        }
    }
    return netMomentum;
}

void System::destroyComVel()
{
    arma::vec netMomentum = netMom();

    for(unsigned int i = 0; i< size(); i++){
        arma::vec v = -1.0*netMomentum/(m_atoms.at(i)->m()*totalSize());
        m_atoms.at(i)->addV(v);
    }
    if(m_systemParameters.rpmd == true){
        for (unsigned int i = 0; i< ringPolymerSize(); i++){
            arma::vec v = -1.0*netMomentum/(m_ringPolymerBeads.at(i)->m()*totalSize());
            m_ringPolymerBeads.at(i)-> addV(v);
        }
    }
}

System::~System()
{
    if (m_box != NULL)
    {
        delete m_box;
    }
    if (m_systemConnectivity != NULL)
    {
        delete m_systemConnectivity;
    }
    if (m_systemIntegrator != NULL)
    {
        delete m_systemIntegrator;
    }
    if (m_systemGeneralMd != NULL)
    {
        delete m_systemGeneralMd;
    }
    if (m_systemThermostat !=NULL)
    {
        delete m_systemThermostat;
    }
    for(unsigned int i= 0; i < m_molecules.size(); i++){
        if (m_molecules.at(i) != NULL){
            delete m_molecules[i];
        }
    }
    if (m_piezo != NULL)
    {
      delete m_piezo;
    }
}

vector<Molecule*>& System::molecules()
{
    return m_molecules;
}

std::vector<Atom *> &System::atoms()
{
    return m_atoms;
}

std::vector<RingPolymer *> &System::ringPolymers()
{
    return m_ringPolymers;
}


vector<Atom*> & System::ringPolymerBeads()
{
    return m_ringPolymerBeads;
}


void System::newAtom(Molecule& molecule, string name, double x, double y, double z,
                     double vx, double vy, double vz,
                     double q, double m)
{
    Atom& atom = molecule.newAtom(name, x, y, z, vx, vy, vz, q, m);
    m_atoms.push_back(&atom);
    m_size += 1;
}

SimulationParameters& System::systemParameters()
{
    return m_systemParameters;
}

const int System::numMol()
{
    return m_molecules.size();
}

Molecule& System::lastMol()
{
    return *m_molecules.at(numMol()-1);
}

Box& System::getBox()
{
    return *m_box;
}

void System::setBox(arma::mat &matrix)
{
    if (m_box != NULL)
    {
        delete m_box;
    }
    m_box = new Box(matrix);
}

void System::setBoxTriclinic(arma::mat &matrix)
{
    if (m_box != NULL)
    {
        delete m_box;
    }
    m_box = new BoxTriclinic(matrix);
    parley("setting triclinic box");
}

void System::setBoxRectangular(arma::mat &matrix)
{
    if (m_box != NULL)
    {
        delete m_box;
    }
    m_box = new BoxRectangular(matrix);
    parley("setting rectangular box");
}

Integrator& System::getIntegrator()
{
    return *m_systemIntegrator;
}

GeneralMd& System::getGeneralMd()
{
    return *m_systemGeneralMd;
}

Piezo& System::getPiezo()
{
    return *m_piezo;
}

TimeDepField& System::getTimeDepField()
{
  return *m_tdField;
}
const std::string& System::getHessianFile()
{
    return m_hessianFile;
}

const std::string& System::getDipoleDerivFile()
{
    return m_dipoleDerivFile;
}

void System::setPiezo()
{
  m_piezo = new Piezo(*this);
}

void System::setTimeDepField()
{
  m_tdField = new TimeDepField(*this);
}


void System::calculateRotTensor()
{
    m_rotTensor = arma::mat(3,3);
    m_rotTensor.zeros();


    for(unsigned int i=0; i<size(); i++){

        arma::vec relPos = m_atoms.at(i)->r()-m_geomCen;
        arma::mat atomTensor(3,3);
        atomTensor.zeros();
        atomTensor(0,0)= relPos(1)*relPos(1) + relPos(2)*relPos(2);
        atomTensor(1,1)= relPos(0)*relPos(0) + relPos(2)*relPos(2);
        atomTensor(2,2)= relPos(0)*relPos(0) + relPos(1)*relPos(1);
        atomTensor(0,1)= -relPos(0)*relPos(1);
        atomTensor(1,0)= atomTensor(0,1);
        atomTensor(0,2)= -relPos(0)*relPos(2);
        atomTensor(2,0)= atomTensor(0,2);
        atomTensor(1,2)= -relPos(1)*relPos(2);
        atomTensor(2,1)= atomTensor(1,2);

        m_rotTensor +=atomTensor;
    }
//    m_rotTensor.print();

}


void System::calculateRotTrans()
{
    arma::vec eigval;
    arma::mat eigvec;

    double thresh = 1e-5;
    arma::eig_sym(eigval, eigvec,m_rotTensor);

    for(int i=2; i>=0; i--){
        if (abs(eigval(i)) < thresh){
            eigvec.shed_col(i);
        }
    }
    int numberCol = 3 + eigvec.n_cols;

    m_rotTrans = arma::mat(size() * 3, numberCol);
    m_rotTrans.zeros();
    for( unsigned int i=0; i<3; i++){
        for ( unsigned int j=0; j<size(); j++)
        {
            m_rotTrans(3*j+i, i) = 1.0/sqrt(size());

        }
    }
    for (unsigned int i=0; i<eigvec.n_cols; i++){
        for (unsigned int j= 0; j<size(); j++){
            arma::vec relPos = m_atoms.at(j)->r()-m_geomCen;
            m_rotTrans(arma::span(j*3,j*3+2),3+i)= arma::cross(relPos, eigvec(arma::span(0,2),i));
        }
        double scale = arma::norm(m_rotTrans.col(3+i));
        m_rotTrans.col(3+i) = m_rotTrans.col(3+i)/scale;
    }

    //arma::mat eye = m_rotTrans.t()* m_rotTrans;
    //eye.print();

//    m_rotTrans.print();

}

arma::vec System::getGeomCen()
{
    return m_geomCen;

}


void System::calculateGeomCen()
{
    m_geomCen = arma::vec(3);
    m_geomCen.zeros();

    for(unsigned int i = 0; i< size();i++){
    m_geomCen += m_atoms.at(i)->r();

    }
    m_geomCen = m_geomCen/size();
//    m_geomCen.print();
}

arma::mat System::getRotTrans()
{
    return m_rotTrans;
}

arma::mat System::getRotTensor()
{
    return m_rotTensor;
}

void System::setTempKin()
{
    m_kineticEnergy = getGeneralMd().getSystemKineticEnergy();

    m_temperature = 2.0/3.0 * m_kineticEnergy/(systemParameters().k *totalSize());
}

double System::getTemperature()
{
    return m_temperature;
}

double System::getKineticEnergy()
{
    return m_kineticEnergy;
}

void System::setIntegrator()
{
    if (m_systemIntegrator != NULL){
        cerr << "Shiver me timbers! Integrator already set" << endl;
        throw -1;
    }
    if (m_systemParameters.velocityVerlet == true){
        m_systemIntegrator = new VelocityVerlet(*this);
        parley("setting integrator to Velocity Verlet");
    }
}

void System::setThermostat()
{
    m_systemThermostat->setThermostat();
}

Thermostat& System::getThermostat()
{
    return *m_systemThermostat;
}

unsigned int System::size()
{
    return m_size;
}

unsigned int System::ringPolymerSize()
{
    return m_ringPolymerSize;
}

unsigned int System::totalSize()
{
    unsigned int totSize = m_size + m_ringPolymerSize;
    return totSize;
}

Molecule& System::newMolecule(const string& filename)
{
    Molecule* molecule = new Molecule();
    XYZReader reader(filename);
    reader.readFrame(*molecule);
    m_molecules.push_back(molecule);
    m_size += m_molecules.at(m_molecules.size() - 1)->size();
    return *m_molecules.at(m_molecules.size() - 1);
}

Molecule& System::newMolecule(int N)
{
    Molecule* molecule = new Molecule();
    for(int i=0; i<N; i++){
        molecule->newAtom();
    }
    for(unsigned int i = 0; i < molecule->atoms().size(); i++){
        m_atoms.push_back(molecule->atoms().at(i));
    }
    m_molecules.push_back(molecule);
    m_size += m_molecules.at(m_molecules.size() - 1)->size();
    return *m_molecules.at(m_molecules.size() - 1);
}

Molecule& System::newMolecule()
{
    Molecule* molecule = new Molecule();
    m_molecules.push_back(molecule);
    m_size += m_molecules.at(m_molecules.size() - 1)->size();
    return *m_molecules.at(m_molecules.size() - 1);
}

RingPolymer& System::newRingPolymer(unsigned int numBeads, double partMass, double fictMass, double partCharge, double temp, double cenX, double cenY, double cenZ)
{
    RingPolymer* ringPolymer = new RingPolymer(numBeads, partMass, fictMass, partCharge, temp, cenX, cenY, cenZ);
    //m_ringPolymers.push_back(RingPolymer(numBeads, partMass, partCharge, temp, cenX, cenY, cenZ));
    //RingPolymer& ringPolymer = m_ringPolymers.at(m_ringPolymers.size() - 1);
    m_ringPolymers.push_back(ringPolymer);
    ringPolymer->initialize();
    m_ringPolymerSize += ringPolymer->beadNumber();
    for(unsigned int i = 0; i< ringPolymer->beadNumber(); i++)
    {
        //Atom * atom = ringPolymer->beads().at(i);
        m_ringPolymerBeads.push_back(ringPolymer->beads().at(i));
    }

    //cout << ringPolymer.beads().at(ringPolymer.beadNumber() - 1)->m() << endl;
    //cout << m_ringPolymerBeads.at(m_ringPolymerBeads.size() - 1)->m() << endl;

    return *ringPolymer;
/*
    cout << m_ringPolymerBeads.size() << endl;
    cout << ringPolymer.beadNumber() << endl;
    for(unsigned int i = 0; i<ringPolymer.beadNumber(); i++)
    {
        Atom * atom1 = ringPolymer.beads().at(i);
        Atom * atom2 = m_ringPolymerBeads.at(i);
        cout << "atom1 " << atom1 << " ";
        cout << "atom2 " << atom2 << " " << (atom1 == atom2);
        cout << endl;
    }
*/
}

Connectivity& System::systemConnectivity()
{
    return *m_systemConnectivity;
}

std::ostream& operator<<(std::ostream& stream,System& system)
{
    return stream << system.m_molecules;
}

void System::parley(std::string message)
{
    std::cout << "Captain Luffy says: " << message << endl;
}

std::string& System::getInputFile()
{
    return m_inputFile;
}

void System::setHessianFile(const std::string& hessianFile)
{
  m_hessianFile = hessianFile;
}

void System::setDipoleDerivFile(const std::string& dipDivFile)
{
  m_dipoleDerivFile = dipDivFile;
}
