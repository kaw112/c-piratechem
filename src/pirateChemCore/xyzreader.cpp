#include "xyzreader.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/utility.hpp>

using namespace std;

XYZReader::XYZReader(const string &filename)
{
    m_handle.open(filename.c_str());
    if(! m_handle)
    {
        throw std::ios_base::failure("Error opening input file");
    }

}

bool XYZReader::readFrame(Molecule& molecule)
{
    molecule.clear();

    //! storage line
    string line = "";

    //! gets atom count
    getline(m_handle, line);

    //! abort reading if at end of file
    if (m_handle.eof()) {
        return false;
    }
    if (m_handle.bad() || m_handle.fail()) {
        throw ios_base::failure("error reading count line");
    }
    int count  = boost::lexical_cast<int>(line);

    //! gets comment line
    getline(m_handle, line);
    if (m_handle.eof() || m_handle.bad() || m_handle.fail()) {
        throw ios_base::failure("error reading comment line");
    }

    for (int i = 0; i < count; i++)
    {
         //! gets each atom line
        getline(m_handle, line);
        if (m_handle.eof() || m_handle.bad() || m_handle.fail()) {
            cerr << "atom line: " << i << endl;
            throw ios_base::failure("error reading atom line");
        }

         //! split line into tokens
        vector<string> tokens;
        boost::split(tokens,line, boost::is_any_of(" "));

        //! convert tokens to doubles
        string N = tokens.at(0);
        double x = boost::lexical_cast<double>(tokens.at(1));
        double y = boost::lexical_cast<double>(tokens.at(2));
        double z = boost::lexical_cast<double>(tokens.at(3));

        //! make new atom
        molecule.newAtom(N, x, y, z);
    }
    return true;
}
