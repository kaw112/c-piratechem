#include "constants.h"

void createMassMap()
{
  std::map <std::string, double> m;

  m["h"] =  1.00794;
  m["c"] =  12.0107;
  m["n"] =  14.00674;
  m["o"] =  15.9994;

  massMap = m;
}
