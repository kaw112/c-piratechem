#include "connectivity.h"
#include "psystem.h"
//#include <armadillo>
#include "box.h"

using namespace std;

LJ::LJ(int index1, int index2, double sigma, double epsilon)
{
    atom1 = index1;
    atom2 = index2;
    s = sigma;
    e = epsilon;
}

bool operator==(const LJ& lj_1, const LJ& lj_2)
{
    if (((lj_1.atom1 == lj_2.atom1) && (lj_1.atom2 == lj_2.atom2)) ||
        ((lj_1.atom1 == lj_2.atom2) && (lj_1.atom2 == lj_2.atom1))) {
        return true;
    }
    return false;
}

bool operator!=(const LJ& lj_1, const LJ& lj_2)
{
    return !(lj_1 == lj_2);
}

Spring::Spring(int index1, int index2, double k, double r0)
{
    atom1 = index1;
    atom2 = index2;
    mk = k;
    mr0 = r0;
}


bool operator==(const Spring& spring_1, const Spring& spring_2)
{
    if (((spring_1.atom1 == spring_2.atom1) && (spring_1.atom2 == spring_2.atom2)) ||
        ((spring_1.atom1 == spring_2.atom2) && (spring_1.atom2 == spring_2.atom1))) {
        return true;
    }
    return false;
}

bool operator!=(const Spring& spring_1, const Spring& spring_2)
{
        return !(spring_1 == spring_2);
}

Coulomb::Coulomb(int index1, int index2)
{
    atom1 = index1;
    atom2 = index2;
}

bool operator==(const Coulomb& coulomb_1, const Coulomb& coulomb_2)
{
    if (((coulomb_1.atom1 == coulomb_2.atom1) && (coulomb_1.atom2 == coulomb_2.atom2)) ||
        ((coulomb_1.atom1 == coulomb_2.atom2) && (coulomb_1.atom2 == coulomb_2.atom1))) {
        return true;
    }
    return false;
}

bool operator!=(const Coulomb& coulomb_1, const Coulomb& coulomb_2)
{
     return !(coulomb_1 == coulomb_2);
}

Connectivity::Connectivity(System &system) : m_system(system)
{
}

vector<LJ>& Connectivity::conLJ()
{
      return m_conLJ;
}

void Connectivity::checkConLJ()
{
    m_conLJcutoff.clear();
    for(unsigned int i = 0; i < m_conLJ.size(); i++){
        if (arma::norm(m_system.getBox().dr(
                m_system.atoms().at(m_conLJ.at(i).atom1)->r(),
                m_system.atoms().at(m_conLJ.at(i).atom2)->r()), 2)
                < m_system.systemParameters().lj_cutoff){
            m_conLJcutoff.push_back(&m_conLJ[i]);

        }
    }

}

std::vector<Spring> &Connectivity::conSpring()
{
      return m_conSpring;
}

vector<Coulomb>& Connectivity::conCoulomb()
{
      return m_conCoulomb;
}

void Connectivity::addConLJ(LJ& lj)
{
    for (unsigned int i = 0; i < m_conLJ.size(); i++) {
        LJ& other_lj = m_conLJ.at(i);
        if (other_lj == lj) {
            cerr << "LJ1: " << lj.atom1 << " " << lj.atom2 << endl;
            cerr << "LJ2: " << other_lj.atom1 << " " << other_lj.atom2 << endl;
            m_system.parley("lj already defined!");
            throw "bunnies";
        }
    }
    m_conLJ.push_back(lj);
}

void Connectivity::addConSpring(Spring& spring)
{
    for (unsigned int i = 0; i < m_conSpring.size(); i++) {
        Spring& other_spring = m_conSpring.at(i);
        if (other_spring == spring) {
            cerr << "Spring1: " << spring.atom1 << " " << spring.atom2 << endl;
            cerr << "Spring2: " << other_spring.atom1 << " " << other_spring.atom2 << endl;
            m_system.parley("spring already defined!");
            throw "bunnies";
        }
    }
    m_conSpring.push_back(spring);
}

void Connectivity::addConCoulomb(Coulomb &coulomb)
{
    for (unsigned int i = 0; i < m_conCoulomb.size(); i++) {
        Coulomb& other_coulomb = m_conCoulomb.at(i);
        if (other_coulomb == coulomb) {
            cerr << "Coulomb1: " << coulomb.atom1 << " " << coulomb.atom2 << endl;
            cerr << "Coulomb2: " << other_coulomb.atom1 << " " << other_coulomb.atom2 << endl;
            m_system.parley("coulomb already defined!");
            throw "bunnies";
        }
    }
    m_conCoulomb.push_back(coulomb);
}

void Connectivity::standardConLJ()
{
    if (m_conLJ.size() != 0){
        cerr << "Dead men tell no tales; LJ connectivity vector already set" << endl;
        throw -1;
    }
    for(unsigned int i = 0; i < m_system.size() - 1; i ++){
        for (unsigned int j = i + 1; j < m_system.size(); j++){
            LJ lj(i,j);
            addConLJ(lj);
        }
    }
}

void Connectivity::standardConSpring()
{
    if (m_conSpring.size() != 0){
        cerr << "Dead men tell no tales; Spring connectivity vector already set" << endl;
        throw -1;
    }
    for(unsigned int i = 0; i < m_system.size() - 1; i ++){
        for (unsigned int j = i + 1; j < m_system.size(); j++){
            Spring spring(i,j);
            addConSpring(spring);
        }
    }
}

void Connectivity::standardConCoulomb()
{
    if (m_conCoulomb.size() != 0){
        cerr << "Dead men tell no tales; Coulomb connectivity vector already set" << endl;
        throw -1;
    }
    for(unsigned int i = 0; i < m_system.size() - 1; i ++){
        for (unsigned int j = i + 1; j < m_system.size(); j++){
            Coulomb coulomb(i,j);
            addConCoulomb(coulomb);
        }
    }
}
