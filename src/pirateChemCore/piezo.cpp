#include "piezo.h"
#include "psystem.h"
#include "constants.h"
#include <boost/lexical_cast.hpp>
#include <armadillo>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include<boost/utility.hpp>

Piezo::Piezo(System &system) : m_system(system)
{
    m_hessRegex = boost::regex("hessian");
    m_dipDerivRegex = boost::regex("dipole derivative");
    makeHessian(m_system.getHessianFile());
    makeDipoleDeriv(m_system.getDipoleDerivFile());
    m_initialPositions = obtainPositions();
    //m_initialPositions.print();

}
Piezo::~Piezo()
{


}

arma::mat Piezo::piezoForces()
{
  arma::vec disp = angToBohr*(obtainPositions()-m_initialPositions);

  arma::mat transformMat = calculateVibBasis();

  arma::mat subHess = transformMat.t() * m_hessian* transformMat;
  arma::mat subDipoleDeriv = transformMat.t() * m_dipoleDeriv;
  arma::vec subDisp = transformMat.t() * disp;
  arma::vec field = m_system.getTimeDepField().calcTotalSineField(m_system.time());

  arma::vec forces = -transformMat* subHess*subDisp-transformMat*subDipoleDeriv*field;
  //std::cout<< forces.size()<<std::endl;
  //std::cout << m_system.size() <<std::endl;
  //std::cout <<"file: "<<__FILE__<<"  line: "<<__LINE__<<std::endl;
  arma::mat newforces= arma::mat(forces);
  newforces.reshape(3,m_system.size());
  //std::cout <<"file: "<<__FILE__<<"  line: "<<__LINE__<<std::endl;
  return newforces;
}
arma::vec Piezo::obtainPositions()
{
  arma::vec positions = arma::vec(m_system.size()*3);
  for (unsigned int i = 0; i<m_system.size(); i++)
    {
      positions(3*i)    = m_system.atoms().at(i)->r()(0);
      positions(3*i +1) = m_system.atoms().at(i)->r()(1);
      positions(3*i +2) = m_system.atoms().at(i)->r()(2);
    }
  return positions;
}
void Piezo::makeHessian(const std::string& hessianFile)
{

    m_hessian = arma::mat(m_system.size()*3,m_system.size()*3);

    m_hessian.zeros();

    std::fstream handle;
    handle.open(hessianFile.c_str());
    if(!handle){
        std::cerr << "Arrr... I can't parlay the hessian file" << std::endl;
        throw -1;
    }
        std::string line = "";
        std::getline(handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);

        boost::smatch matches;
        bool foundMatch =  boost::regex_match(line, matches, m_hessRegex);

        if (foundMatch){

          std::cout <<  "Gyarrr  . . . Parsing the hessian file"<<std::endl;
          unsigned int counterRow = 0;
          unsigned int counterColumns = 0;
          while (!handle.eof()){
            std::string nextLine = "";
            std::getline(handle, nextLine);

            boost::algorithm::trim(nextLine);
            boost::algorithm::to_lower(nextLine);
            if (nextLine.size()>0){
              std::vector<std::string> tokens;
              boost::split(tokens,nextLine, boost::is_any_of(" "),boost::token_compress_on);

              for(unsigned int i = 0; i < tokens.size(); i++){
                if (counterColumns > counterRow){
                  counterRow +=1;
                  counterColumns = 0;
                }
                  m_hessian(counterRow, counterColumns)= boost::lexical_cast<double>(tokens.at(i));
                  m_hessian(counterColumns, counterRow) = m_hessian(counterRow, counterColumns);
                  counterColumns += 1;
              }

            }


          }

        }
        else {
          std::cerr << "Stow away aboard . . . hessian file improperly formatted"<<std::endl;
          throw -1;

        }
        m_hessian(84,84)+= 0.0;
        m_hessian(85,85)+= 0.0;
        m_hessian(86,86)+= 0.0;
        m_hessian(84,85)+= 0.0;
        m_hessian(85,84)+= 0.0;
        m_hessian(84,86)+= 0.0;
        m_hessian(86,84)+= 0.0;
        m_hessian(85,86)+= 0.0;
        m_hessian(86,85)+= 0.0;
        m_hessian(87,87)+= 0.0;
        m_hessian(88,88)+= 0.0;
        m_hessian(89,89)+= 0.0;
        m_hessian(87,88)+= 0.0;
        m_hessian(88,87)+= 0.0;
        m_hessian(88,89)+= 0.0;
        m_hessian(89,88)+= 0.0;
        m_hessian(87,89)+= 0.0;
        m_hessian(89,87)+= 0.0;
}



 void Piezo::makeDipoleDeriv(const std::string& dipoleDerivFile)
{

     m_dipoleDeriv = arma::mat(m_system.size()*3, 3);

     m_dipoleDeriv.zeros();

     std::fstream handle;
     handle.open(dipoleDerivFile.c_str());
     if(!handle){
         std::cerr << "Arrr... I can't parlay the dipole derivative file" << std::endl;
         throw -1;
     }
     std::string line = "";
     std::getline(handle, line);

     boost::algorithm::trim(line);
     boost::algorithm::to_lower(line);

     boost::smatch matches;
     bool foundMatch =  boost::regex_match(line, matches, m_dipDerivRegex);

     if (foundMatch){

        std::cout <<  "Gyarrr  . . . Parsing the dipole derivative file"<<std::endl;

        unsigned int counterRow = 0;
        while (!handle.eof()){
            std::string nextLine = "";
            std::getline(handle, nextLine);

            boost::algorithm::trim(nextLine);
            boost::algorithm::to_lower(nextLine);
            if (nextLine.size()>0){
               std::vector<std::string> tokens;
               boost::split(tokens,nextLine, boost::is_any_of(" "),boost::token_compress_on);

               for(unsigned int i = 0; i < tokens.size(); i++){

                   m_dipoleDeriv(counterRow, i)= boost::lexical_cast<double>(tokens.at(i));

               }
               counterRow += 1;

             }


           }

         }
         else {
           std::cerr << "Stow away aboard . . . dipole derivative file improperly formatted"<<std::endl;
           throw -1;

         }
}

arma::mat Piezo::calculateVibBasis()
{
  m_system.calculateGeomCen();
  m_system.calculateRotTensor();
  m_system.calculateRotTrans();

  arma::mat rt = m_system.getRotTrans();

  arma::vec eigval;
  arma::mat eigvec;

  double thresh = 1e-5;

  arma::eig_sym(eigval, eigvec,m_hessian);
  // project out rotations and translations;
  #pragma omp parallel for
  for (unsigned int i = 0; i< eigvec.n_cols;i++){
    for(unsigned int j = 0; j< rt.n_cols; j++){
      eigvec.col(i)= eigvec.col(i)-arma::dot(eigvec.col(i),rt.col(j))*rt.col(j);
    }
  }

  // delete eigenvectors that are zero
  for (int i=eigvec.n_cols-1; i>=0; i--){
    arma::vec a = arma::abs(eigvec.col(i));
    double max = arma::max(a);
    if(max<thresh){
      eigvec.shed_col(i);
    }
  }

  // normalize
  #pragma omp parallel for
  for (unsigned int i= 0 ; i<eigvec.n_cols;i++){
    double scale = arma::norm(eigvec.col(i));
    eigvec.col(i)= eigvec.col(i)/scale;
  }

  //gram-schmidt procedure
  for(unsigned int i = 0; i<eigvec.n_cols; i++){
    for (unsigned int j = 0; j<i; j++){
      eigvec.col(i) = eigvec.col(i)-arma::dot(eigvec.col(i),eigvec.col(j))* eigvec.col(j);
    }
    double max = arma::max(arma::abs(eigvec.col(i)));
    if (max<thresh){
      eigvec.col(i)= arma::vec(eigvec.n_rows).zeros();
    }
    else {
      double scale = arma::norm(eigvec.col(i));
      eigvec.col(i)= eigvec.col(i)/scale;
    }
  }

  // delete vectors that are zero
  for (int i=eigvec.n_cols-1; i>=0; i--){
    arma::vec a = arma::abs(eigvec.col(i));
    double max = arma::max(a);
    if(max<thresh){
      eigvec.shed_col(i);
    }
  }
  //std::cout << eigvec.n_cols << std::endl;
return eigvec;
}

arma::mat& Piezo::getHessian()
{
  return m_hessian;
}

arma::mat& Piezo::getDipoleDeriv()
{
  return m_dipoleDeriv;
}
