#include "parameters.h"


#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/utility.hpp>
#include <exception>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

SimulationParameters::SimulationParameters() {
    m_regexTrue = boost::regex("true");
    m_regexFalse = boost::regex("false");

    temperature = 300.0;
    fieldExt = 0.00;
    lj_cutoff = 12 * 12;
    lj = false;
    piezo = false;
    spring = false;
    coulomb = false;
    field = false;
    ljConStandard = false;
    springConStandard = false;
    coulombConStandard = false;
    maxwellVelocities  = false;
    randomPositions = false;
    uniformPositions = false;
    velocityVerlet = true;
    noseHoover = false;
    langevinTherm =false;
    bussiTherm = false;
    pbc = false;
    saveEnergy = false;
    saveTrajectory = true;
    rpmd = false;
    uniformSystem = false;
    uniformMass   = 1.0;
    dt = 1; //! femto secs
    numSteps = 10000;
    printStep = 10;
    outWidth = 23;
    outPrecision = 15;
    outStub = "out";
    uniformName = "C";
    uniformSystemSize = 10;
    currentStep = 0;
    k = 3.166811429e-6; //! Hartree/K
    //angToBohr = 1.889725989; //! bohr/ang
    //BohrToAng = .52917724888; //! ang/bohr
    //amuToMe =  1822.8884864247; //! Me/amu
    //meToAmu = .0005485799;  //! amu/Me
    //femtosecToAu = 41.341373336561; //! au./femtosecond
    bussiTau =  10; //!femtoseconds
    pi = atan(1)*4;
    pfs = .0055263496; //! e**2/(eV*Ang)
    noseHooverM = 10;
    randomseed = 0;
}

void SimulationParameters::lineParse(std::string& input_line)
{
    //! comments are for LOSERs
    input_line = boost::regex_replace(input_line, m_regexFalse, "0");
    input_line = boost::regex_replace(input_line, m_regexTrue, "1");

    //! split into tokens
    vector<string> tokens;
    boost::split(tokens, input_line, boost::is_any_of("="));

    //! trim each token
    for (unsigned int i = 0; i < tokens.size(); i++) {
        boost::algorithm::trim(tokens[i]);
    }

    //! parse key = value pair
    if (tokens.size() >= 2) {
        //! look for key
        if (tokens[0] == "temperature") {
            temperature = boost::lexical_cast<double>(tokens[1]);
        }
        else if (tokens[0] == "fieldext") {
            fieldExt = boost::lexical_cast<double>(tokens[1]);
        }
        else if (tokens[0] == "lj") {
            lj = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "piezo") {
            piezo = boost::lexical_cast<bool>(tokens[1]);
        }
        else if(tokens[0] == "pbc"){
            pbc = boost::lexical_cast<bool>(tokens[1]);
        }
        else if(tokens[0] == "randompositions"){
            randomPositions = boost::lexical_cast<bool>(tokens[1]);
        }
        else if(tokens[0] == "uniformpositions"){
            uniformPositions = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "lj_cutoff"){
            lj_cutoff = boost::lexical_cast<double>(tokens[1]);
            lj_cutoff = lj_cutoff * lj_cutoff;
        }
        else if (tokens[0] == "spring") {
            spring = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "coulomb") {
            coulomb = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "field") {
            field = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "ljconstandard"){
            ljConStandard = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "springconstandard"){
            springConStandard = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "coulombconstandard"){
            coulombConStandard = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "maxwellvelocities"){
            maxwellVelocities = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "uniformsystem"){
            uniformSystem = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "uniformmass"){
            uniformMass = boost::lexical_cast<double>(tokens[1]);
        }
        else if (tokens[0] == "uniformsystemsize"){
            uniformSystemSize = boost::lexical_cast<int>(tokens[1]);
        }
        else if (tokens[0] == "dt"){
            dt = boost::lexical_cast<double>(tokens[1]);
        }
        else if (tokens[0] == "numsteps"){
            numSteps = boost::lexical_cast<int>(tokens[1]);
        }
        else if (tokens[0] == "printstep"){
            printStep = boost::lexical_cast<double>(tokens[1]);
        }
        else if (tokens[0] == "nosehoover"){
            noseHoover = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "velocityverlet"){
            velocityVerlet = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "savetrajectory"){
            saveTrajectory = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "saveenergy"){
            saveEnergy = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "rpmd"){
            rpmd = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "outstub"){
            outStub = boost::lexical_cast<std::string>(tokens[1]);
        }
        else if (tokens[0] == "uniformname"){
            uniformName = boost::lexical_cast<std::string>(tokens[1]);
        }
        else if (tokens[0] == "outprecision"){
            outPrecision = boost::lexical_cast<double>(tokens[1]);
        }
        else if (tokens[0] == "outwidth"){
            outWidth = boost::lexical_cast<double>(tokens[1]);
        }
        else if (tokens[0] == "nosehooverm"){
            noseHooverM = boost::lexical_cast<int>(tokens[1]);
        }
        else if (tokens[0] == "randomseed"){
            randomseed = boost::lexical_cast<unsigned int>(tokens[1]);
        }
        else if (tokens[0] == "langevintherm"){
            langevinTherm  = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "bussitherm"){
            bussiTherm  = boost::lexical_cast<bool>(tokens[1]);
        }
        else if (tokens[0] == "bussitau"){
            bussiTau = boost::lexical_cast<double>(tokens[1]);
        }
        else {
            cerr << "warning: token not recognized in input file" << endl;
            cerr << '\t' << input_line << endl;
            throw "kittens";
        }
    }
    else {
        cerr << "warning: line not understood in input file" << endl;
        cerr << '\t' << input_line << endl;
        throw "puppies";
    }
}
bool SimulationParameters::valid() {
    if (temperature < 0) {
        cerr << "temperature is negative: " << temperature << endl;
        return false;
    }
    return true;
}

std::ostream& operator<<(std::ostream& stream,  SimulationParameters& parameters)
{
    boolalpha(stream);
    stream << "temperature is " << parameters.temperature << endl
           << "external field is " << parameters.fieldExt << endl
           << "ljs cut off distance is " << parameters.lj_cutoff << endl
           << "field is " << parameters.field << endl
           << "spring is " << parameters.spring << endl
           << "ljs is " << parameters.lj << endl
           << "coulomb is " << parameters.coulomb << endl
           << "ljConStandard is " << parameters.ljConStandard << endl
           << "springConStandard is " << parameters.springConStandard << endl
           << "coulombConStandard is " << parameters.coulombConStandard << endl
           << "time step is " << parameters.dt << " femto seconds" << endl
           << "print every " << parameters.printStep << " steps" << endl
           << "number of iterations is " << parameters.numSteps << endl
           << "velocity verlet integrator method is " << parameters.velocityVerlet << endl
           << "Nose Hoover thermostat is " << parameters.noseHoover << endl
           << "Intitial Maxwell Velocities is " << parameters.maxwellVelocities << endl
           << "PBC is set to " << parameters.pbc << endl
           << "saveEnergy is " << parameters.saveEnergy << endl
           << "saveTrajectory " << parameters.saveTrajectory << endl;
    return stream;

}
