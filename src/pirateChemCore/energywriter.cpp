#include "energywriter.h"
#include "generalmd.h"
#include "psystem.h"
#include "integrator.h"
#include <iomanip>

using namespace std;

EnergyWriter::EnergyWriter(System &system, const string &filename) : m_system(system)
{
    m_handle.open(filename.c_str(), ios::app);
    if(! m_handle)
    {
        throw std::ios_base::failure("Error opening output file");
    }

    width = m_system.systemParameters().outWidth;
    pre = m_system.systemParameters().outPrecision;

    m_handle << setw(width) << "Step"            <<
                setw(width) << "Temperature"     <<
                setw(width) << "TotalEnergy"     <<
                setw(width) << "KineticEnergy"   <<
                setw(width) << "PotentialEnergy" <<
                setw(width) << "CoulombEnergy"   <<
                setw(width) << "SpringEnergy"    <<
                setw(width) << "LJEnergy"        << endl;
}

void EnergyWriter::writeFrame()
{
    double T = m_system.getKineticEnergy();

    m_handle << setw(width) << setprecision(pre) << m_system.systemParameters().currentStep
             << setw(width) << setprecision(pre) << m_system.getTemperature()
    << setw(width) << setprecision(pre) << m_system.getIntegrator().getTotalEnergy() + T
             << setw(width) << setprecision(pre) << T
             << setw(width) << setprecision(pre) << m_system.getIntegrator().getTotalEnergy()
             << setw(width) << setprecision(pre) << m_system.getIntegrator().getCoulombEnergy()
             << setw(width) << setprecision(pre) << m_system.getIntegrator().getSpringEnergy()
             << setw(width) << setprecision(pre) << m_system.getIntegrator().getLjEnergy() << endl;
}
