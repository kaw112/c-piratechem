#include "waveForm.h"
#include <armadillo>
#include <cmath>


SineWave::SineWave(double phaseFactor, double wvX, double wvY, double wvZ, double frequency)
{
  m_phase = phaseFactor;
  m_wvX   = wvX;
  m_wvY   = wvY;
  m_wvZ   = wvZ;
  m_freq  = frequency;
}


arma::vec SineWave::getField(double time)
{
  double PI = 3.14159265;
  arma::vec field = arma::zeros<arma::vec>(3);
  field[0] = m_wvX * sin(2*PI*m_freq*time+m_phase);
  field[1] = m_wvY * sin(2*PI*m_freq*time+m_phase);
  field[2] = m_wvZ * sin(2*PI*m_freq*time+m_phase);
  return field;
}

double SineWave::getFreq()
{
  return m_freq;
}

arma::vec SineWave::getWaveVector()
{
  arma::vec waveVec = arma::zeros<arma::vec>(3);
  waveVec[0]= m_wvX;
  waveVec[1]= m_wvY;
  waveVec[2]= m_wvZ;
  return waveVec;
}

double SineWave::getPhase()
{
  return m_phase;
}
