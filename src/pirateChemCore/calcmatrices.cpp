#include "calcmatrices.h"
#include "psystem.h"
#include "box.h"

using namespace std;

CalcMatrices::CalcMatrices(System& system) : m_system(system)
{
}

arma::mat& CalcMatrices::posVecMatrix(arma::mat &posVecMat)
{
    //arma::mat posVecMat(3, m_system.size());
    for(unsigned int i = 0;i < m_system.size(); i++){
        posVecMat.col(i) = m_system.atoms().at(i)->r();
    }
    return posVecMat;

}

arma::mat& CalcMatrices::ljDiffMatrix(vector<LJ*>& conLJcutoff, arma::mat& ljDiffMat)
{
    //arma::mat ljDiffMat(3, conLJcutoff.size());
    for(unsigned int i = 0; i < conLJcutoff.size(); i++){
        ljDiffMat.col(i) = m_system.getBox().dr(m_system.atoms().at(conLJcutoff.at(i)->atom1)->r(),
                                                m_system.atoms().at(conLJcutoff.at(i)->atom2)->r());
    }
    return ljDiffMat;
}

arma::mat& CalcMatrices::coulomDiffMatrix(vector<Coulomb>& conCoulomb, arma::mat &coulombDiffMat)
{
    //arma::mat coulombDiffMat(3, conCoulomb.size());
    for(unsigned int i = 0; i < conCoulomb.size(); i++){
        coulombDiffMat.col(i) = m_system.getBox().dr(m_system.atoms().at(conCoulomb.at(i).atom1)->r(),
                                                m_system.atoms().at(conCoulomb.at(i).atom2)->r());
    }
    return coulombDiffMat;
}

arma::mat& CalcMatrices::springDiffMatrix(vector<Spring>& conSpring, arma::mat& springDiffMat)
{
    //arma::mat springdiffMat(3, conSpring.size());
    for (unsigned int i = 0; i < conSpring.size(); i++){
        springDiffMat.col(i) = m_system.getBox().dr(m_system.atoms().at(conSpring.at(i).atom1)->r(),
                                                m_system.atoms().at(conSpring.at(i).atom2)->r());
    }
    return springDiffMat;
}
