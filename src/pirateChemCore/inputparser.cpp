#include "inputparser.h"
#include "psystem.h"
#include "connectivity.h"
#include "ringpolymer.h"
#include<boost/utility.hpp>
#include <armadillo>

#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
//#include <boost/regex.hpp>


using namespace std;

InputParser::InputParser(System &system) : m_system(system)
{
    initializeSectionMap();
    m_regexComments = boost::regex("\\s*#.*$");
    m_regexPirate = boost::regex("^p\\)\\s*(.+)\\s*$");
    m_regexSpaces = boost::regex("\\s*");

}

void InputParser::initializeSectionMap()
{
    m_sectionMap["molecule"]   = MoleculeSection;
    m_sectionMap["parameters"] = ParametersSection;
    m_sectionMap["springs"]    = SpringsSection;
    m_sectionMap["lennards"]   = LennardsSection;
    m_sectionMap["coulombs"]   = CoulombsSection;
    m_sectionMap["charges"] = ChargesSection;
    m_sectionMap["masses"] = MassesSection;
    m_sectionMap["velocities"] = VelocitiesSection;
    m_sectionMap["rectangular box"] = RecBoxSection;
    m_sectionMap["triclinic box"] = TriBoxSection;
    m_sectionMap["rpmd"] = RpmdSection;
    m_sectionMap["piezo"] = PiezoSection;
    m_sectionMap["time dependent field"] = TimeDepFieldSection;
}

void InputParser::parse(const std::string& inputFile)
{


    m_handle.open(inputFile.c_str());

    if(!m_handle){
        cerr << "Arrr... I can't parlay the input file" << endl;
        throw -1;
    }

    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line); //removes endline
        boost::algorithm::to_lower(line);  //makes line lower case so program is case insensitive
        line = boost::regex_replace(line, m_regexComments, "");  //replaces comments with nothing

        if (line.size() > 0) {
            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];


                if (m_sectionMap.find(keyword) == m_sectionMap.end()) {
                    cerr << keyword << " is not an accepted keyword" << endl;
                    throw "bunnies";
                }

                switch (m_sectionMap.at(keyword))
                {
                    case MoleculeSection:
                        parseMoleculeSection();
                        break;

                    case ParametersSection:
                        parseParameterSection();
                        break;

                    case SpringsSection:
                        parseSpringsSection();
                        break;

                    case LennardsSection:

                        parseLennardsSection();
                        break;

                    case CoulombsSection:
                        parseCoulombsSection();
                        break;

                    case VelocitiesSection:
                        parseVelocitiesSection();
                        break;

                    case MassesSection:
                        parseMassesSection();
                        break;

                    case ChargesSection:
                        parseChargesSection();
                        break;

                    case RecBoxSection:
                        parseRectangularSection();
                        break;

                    case TriBoxSection:
                    parseTriclinicSection();
                        break;

                    case RpmdSection:
                    parseRpmdSection();
                        break;

                    case PiezoSection:
                    parsePiezoSection();
                        break;

                    case TimeDepFieldSection:
                    parseTimeDepFieldSection();
                        break;

                    default:
                        cerr << "Stow away aboard ... section title " << keyword << "is not known" << endl;
                        throw -1;
                        break;
                }
            }
            else
            {
                cerr << "unknown line in input file me matey" << endl;
                cerr << "line: " << line << endl;
                throw -1;
            }
        }
    }

    m_handle.close();
}

void InputParser::parseMoleculeSection()
{
    Molecule& molecule = m_system.newMolecule();

    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {

            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                string N = tokens.at(0);
                double x = boost::lexical_cast<double>(tokens.at(1));
                double y = boost::lexical_cast<double>(tokens.at(2));
                double z = boost::lexical_cast<double>(tokens.at(3));

                //! make new atom
                m_system.newAtom(molecule, N, x, y, z);

            }
        }
    }
}

void InputParser::parseParameterSection()
{
    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {
            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                m_system.systemParameters().lineParse(line);
            }
        }
    }
}

void InputParser::parseCoulombsSection()
{
    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {
            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                unsigned int index1 = boost::lexical_cast<int>(tokens.at(0));
                unsigned int index2 = boost::lexical_cast<int>(tokens.at(1));

                if (index1 >= m_system.size()) {
                    cerr << "A bit too big in the britches aren't ye, index1 " <<
                            index1 << " not in molecule range" << endl;
                    throw -1;
                }
                if (index2 >= m_system.size()){
                    cerr << "A bit too big in the britches aren't ye, index2 " <<
                            index2 << " not in molecule range" << endl;
                    throw -1;
                }

                //! add to connectivity vector for system
                Coulomb coul(index1, index2);
                m_system.systemConnectivity().addConCoulomb(coul);
            }
        }
    }
}

void InputParser::parseSpringsSection()
{
    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {
            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                unsigned int index1 = boost::lexical_cast<int>(tokens.at(0));
                unsigned int index2 = boost::lexical_cast<int>(tokens.at(1));
                double k = boost::lexical_cast<double>(tokens.at(2));
                double r0 = boost:: lexical_cast<double>(tokens.at(3));

                if (index1 >= m_system.size()) {
                    cerr << "A bit too big in the britches aren't ye, index1 " <<
                            index1 << " not in molecule range" << endl;
                    throw -1;
                }
                if (index2 >= m_system.size()){
                    cerr << "A bit too big in the britches aren't ye, index2 " <<
                            index2 << " not in molecule range" << endl;
                    throw -1;
                }

                //! add to connectivity vector for system
                Spring sprung(index1, index2, k, r0);
                m_system.systemConnectivity().addConSpring(sprung);
            }
        }
    }
}

void InputParser::parseLennardsSection()
{
    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {
            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                unsigned int index1 = boost::lexical_cast<int>(tokens.at(0));
                unsigned int index2 = boost::lexical_cast<int>(tokens.at(1));
                double sigma = boost::lexical_cast<double>(tokens.at(2));
                double epsilon = boost:: lexical_cast<double>(tokens.at(3));

                if (index1 >= m_system.size()) {
                    cerr << "A bit too big in the britches aren't ye, index1 " <<
                            index1 << " not in molecule range" << endl;
                    throw -1;
                }
                if (index2 >= m_system.size()){
                    cerr << "A bit too big in the britches aren't ye, index2 " <<
                            index2 << " not in molecule range" << endl;
                    throw -1;
                }

                //! add to connectivity vector for system
                LJ lj(index1, index2, sigma, epsilon);
                m_system.systemConnectivity().addConLJ(lj);
            }
        }
    }
}

void InputParser::parseVelocitiesSection()
{
    Molecule& molecule = m_system.lastMol();

    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {

            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;                cout << "i am here" << endl;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                unsigned int index = boost::lexical_cast<int>(tokens.at(0));
                double vx = boost::lexical_cast<double>(tokens.at(1));
                double vy = boost::lexical_cast<double>(tokens.at(2));
                double vz = boost::lexical_cast<double>(tokens.at(3));

                if (index >= molecule.size()) {
                    cerr << "A bit too big in the britches aren't ye, index " <<
                            index << " not in molecule range" << endl;
                    throw -1;
                }

                //! make new atom
                molecule.atoms().at(index)->setV(vx, vy, vz);
            }
        }
    }
}

void InputParser::parseMassesSection()
{
    Molecule& molecule = m_system.lastMol();

    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {

            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                unsigned int index = boost::lexical_cast<int>(tokens.at(0));  //atom index
                double mass = boost::lexical_cast<double>(tokens.at(1)); //atom mass

                if (index >= molecule.size()) {
                    cerr << "A bit too big in the britches aren't ye, index " <<
                            index << " not in molecule range" << endl;
                    throw -1;
                }

                //! make new atom
                molecule.atoms().at(index)->setM(mass);
            }
        }
    }
}

void InputParser::parseChargesSection()
{
    Molecule& molecule = m_system.lastMol();

    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {

            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                unsigned int index = boost::lexical_cast<int>(tokens.at(0));  //atom index
                double charge = boost::lexical_cast<double>(tokens.at(1)); //atom charge

                if (index >= molecule.size()) {
                    cerr << "A bit too big in the britches aren't ye, index " <<
                            index << " not in molecule range" << endl;
                    throw -1;
                }

                //! make new atom
                molecule.atoms().at(index)->setQ(charge);
            }
        }
    }
}

void InputParser::parseRectangularSection()
{
    arma::mat matrix(3,3);
    int i = 0;

    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {

            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    m_system.setBoxRectangular(matrix);
                    //m_system.systemParameters().pbc = true;
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                double x = boost::lexical_cast<double>(tokens.at(0));  //atom index
                double y = boost::lexical_cast<double>(tokens.at(1)); //
                double z = boost::lexical_cast<double>(tokens.at(2));


                matrix(0,i) = x;
                matrix(1,i) = y;
                matrix(2,i) = z;

                i++;

            }

        }
    }
}

void InputParser::parseTriclinicSection()
{

    arma::mat matrix(3,3);
    int i = 0;

    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {

            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    m_system.setBoxTriclinic(matrix);
                    m_system.systemParameters().pbc = true;
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //! split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                //! convert tokens to doubles
                double x = boost::lexical_cast<double>(tokens.at(0));  //atom index
                double y = boost::lexical_cast<double>(tokens.at(1)); //
                double z = boost::lexical_cast<double>(tokens.at(2));

                matrix(0,i) = x;
                matrix(1,i) = y;
                matrix(2,i) = z;

                i++;

            }

        }
    }

}

void InputParser::parseRpmdSection()
{
    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        boost::algorithm::to_lower(line);
        line = boost::regex_replace(line, m_regexComments, "");

        if (line.size() > 0) {
            boost::smatch matches;
            bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {
                //!split line into tokens
                vector<string> tokens;
                boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);

                unsigned int beadNumber    = boost::lexical_cast<unsigned int   >(tokens.at(0));
                double partMass   = boost::lexical_cast<double>(tokens.at(1));
                double fictMass   = boost::lexical_cast<double>(tokens.at(2));
                double partCharge = boost::lexical_cast<double>(tokens.at(3));
                double temp       = boost::lexical_cast<double>(tokens.at(4));
                double cenX       = boost::lexical_cast<double>(tokens.at(5));
                double cenY       = boost::lexical_cast<double>(tokens.at(6));
                double cenZ       = boost::lexical_cast<double>(tokens.at(7));

                //! add dat sucka to da system
                m_system.newRingPolymer(beadNumber, partMass, fictMass, partCharge, temp, cenX, cenY, cenZ);
            }
        }
    }
}

void InputParser::parsePiezoSection()
{
    while (!m_handle.eof()) {
        string line = "";
        getline(m_handle, line);

        boost::algorithm::trim(line);
        line = boost::regex_replace(line, m_regexComments, "");


        if (line.size() > 0) {
            boost::smatch matches;

            // need to make line lower case to use regex but keep original unaltered since they contain file names
            string line2 = line;
            boost::algorithm::to_lower(line2);
            bool foundMatch = boost::regex_match(line2, matches, m_regexPirate);

            if (foundMatch) {
                string keyword = matches[1];
                if (keyword == "end") {
                    return;
                }
                else {
                    cerr << "invalid keyword found on lower deck" << endl;
                    throw -1;
                }
            }
            else
            {//! split into tokens
              vector<string> tokens;
              boost::split(tokens, line, boost::is_any_of("="));

              boost::algorithm::to_lower(tokens[0]);
              boost::algorithm::trim(tokens[0]);
              boost::algorithm::trim(tokens[1]);

              if(tokens[0]=="hessian"){
                m_system.setHessianFile(tokens[1]);
              }


              else if(tokens[0]=="dipolederivative"){
                m_system.setDipoleDerivFile(tokens[1]);
              }
              else
              {
                cerr << "gyarrrr ... piezo section be incorectly formatted" << endl;
                throw -1;
              }
            }
          }
    }

}

void InputParser::parseTimeDepFieldSection()
{
  while (!m_handle.eof()) {
    string line = "";
    getline(m_handle, line);
    boost::algorithm::trim(line);
    boost::algorithm::to_lower(line);
    line = boost::regex_replace(line, m_regexComments, "");

    if (line.size() > 0) {
        boost::smatch matches;
        bool foundMatch = boost::regex_match(line, matches, m_regexPirate);

        if (foundMatch) {
            string keyword = matches[1];
            if (keyword == "end") {
                return;
            }
            else {
                cerr << "invalid keyword found on lower deck" << endl;
                throw -1;
            }
        }
        else
        {
            //!split line into tokens
            vector<string> tokens;
            boost::split(tokens,line, boost::is_any_of(" "),boost::token_compress_on);
            if (tokens.size()!=6){
                cerr << "gyarrr ... yo ho ho and a bottle of rum; number of arguments for sine wave object be incorrect" << endl;
              }
            else if (tokens[0]=="sine"){
                double phase = boost::lexical_cast<double>(tokens.at(1));
                double wvx   = boost::lexical_cast<double>(tokens.at(2));
                double wvy   = boost::lexical_cast<double>(tokens.at(3));
                double wvz   = boost::lexical_cast<double>(tokens.at(4));
                double freq  = boost::lexical_cast<double>(tokens.at(5));

                m_system.getTimeDepField().addSineWave(phase, wvx, wvy, wvz, freq);
            }
          }

        }
      }
}
