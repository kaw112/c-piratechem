#include "random.h"
#include <boost/shared_ptr.hpp>
#include <ctime>

namespace PirateChem {

RandomBase::SeedType RandomBase::m_seed = static_cast<RandomBase::SeedType>(time(0));
RandomBase::GeneratorType RandomBase::m_generator = RandomBase::GeneratorType(RandomBase::m_seed);

RandomBase::RandomBase()
{
}

RandomBase::~RandomBase()
{
}

RandomBase::SeedType RandomBase::seed()
{
    return m_seed;
}

void RandomBase::seed(SeedType seed)
{
    if (seed == 0)
    {
        m_seed = static_cast<SeedType>(time(0));
    }
    else
    {
        m_seed = static_cast<SeedType>(seed);
    }
    m_generator.seed(m_seed);
}

std::ostream& operator<<(std::ostream& stream, RandomBase& random)
{
    stream << random.m_seed << ' ';
    stream << random.m_generator;
    return stream;
}

std::istream& operator>>(std::istream& stream, RandomBase& random)
{
    stream >> random.m_seed;
    if (stream.fail() || stream.bad() || stream.eof())
    {
        throw std::runtime_error("can not read state of random number generator");
    }
    stream >> random.m_generator;
    if (stream.fail() || stream.bad() || stream.eof())
    {
        throw std::runtime_error("can not read state of random number generator");
    }
    return stream;
}

Random::Random() : RandomBase()
{
    Distribution dist;
    m_variate = boost::shared_ptr<Variate>(new Variate(m_generator, dist));
}

UniformInt::UniformInt(int low, int high) : RandomBase()
{
    Distribution dist(low, high);
    m_variate = boost::shared_ptr<Variate>(new Variate(m_generator, dist));
}

UniformReal::UniformReal(double low, double high) : RandomBase()
{
    Distribution dist(low, high);
    m_variate = boost::shared_ptr<Variate>(new Variate(m_generator, dist));
}

Normal::Normal(double mu, double sigma) : RandomBase()
{
    Distribution dist(mu, sigma);
    m_variate = boost::shared_ptr<Variate>(new Variate(m_generator, dist));
}

double Random::operator()()
{
    return (*m_variate)();
}

double UniformInt::operator()()
{
    return (*m_variate)();
}

double UniformInt::operator()(int low, int high)
{
    setParameters(low, high);
    return (*m_variate)();
}

double UniformReal::operator()()
{
    return (*m_variate)();
}

double UniformReal::operator()(double low, double high)
{
    setParameters(low, high);
    return (*m_variate)();
}

double Normal::operator()()
{
    return (*m_variate)();
}

double Normal::operator()(double mu, double sigma)
{
    setParameters(mu, sigma);
    return (*m_variate)();
}

void UniformInt::setParameters(int low, int high)
{
    Distribution dist(low, high);
    m_variate.reset(new Variate(m_generator, dist));
}

void UniformReal::setParameters(double low, double high)
{
    Distribution dist(low, high);
    m_variate.reset(new Variate(m_generator, dist));
}

void Normal::setParameters(double mu, double sigma)
{
    Distribution dist(mu, sigma);
    m_variate.reset(new Variate(m_generator, dist));
}

bool Random::chooseFalse(double percent)
{
    if (percent < (*this)()) {
        return false;
    }
    return true;
}

bool Random::chooseTrue(double percent)
{
    if (percent < (*this)()) {
        return true;
    }
    return false;
}

bool Random::metropolis(double deltaE, double beta)
{
    if(deltaE > 0.0)
    {
        if(exp(-deltaE * beta) > (*this)())
        {
            return true;
        }
    }
    else
    {
        return true;
    }
    return false;
}

}
