#include "atom.h"
#include "constants.h"
using namespace std;
#include <boost/algorithm/string.hpp>

Atom::Atom(string name, double x, double y, double z, double vx, double vy, double vz, double q, double m)
{
    boost::algorithm::to_lower(name);
    m_name = name;
    m_q = q;
    m_m=m;
    //m_m = m;
    m_r = arma::zeros<arma::vec>(3);
    m_v = arma::zeros<arma::vec>(3);
    m_r[0] = x;
    m_r[1] = y;
    m_r[2] = z;
    m_v[0] = vx;
    m_v[1] = vy;
    m_v[2] = vz;
    if (m==0.0){
      try {
        cout <<"setting mass"<<endl;
        m_m =massMap[m_name];
      }
      catch(const std::exception& e){
        m_m = m;
      };
    }
}

Atom::~Atom()
{
    cout << "NO SAVE ME" << endl;
}

const arma::vec &Atom::r()
{
    return m_r;
}

const double& Atom::m()
{
    return m_m;
}

const double& Atom::q()
{
    return m_q;
}

const string& Atom::name()
{
    return m_name;
}

void Atom::setR(arma::vec& vec)
{
    m_r = vec;
}

void Atom::setR(double x, double y, double z)
{
    m_r[0] = x;
    m_r[1] = y;
    m_r[2] = z;
}

void Atom::addR(arma::vec &vec)
{
    m_r[0] += vec[0];
    m_r[1] += vec[1];
    m_r[2] += vec[2];
}

void Atom::addR(double x, double y, double z)
{
    m_r[0] += x;
    m_r[1] += y;
    m_r[2] += z;
}

void Atom::scaleR(double s)
{
    m_r[0] *= s;
    m_r[1] *= s;
    m_r[2] *= s;
}

const arma::vec& Atom::v()
{
    return m_v;
}

void Atom::setV(arma::vec &vec)
{
    m_v[0] = vec[0];
    m_v[1] = vec[1];
    m_v[2] = vec[2];
}


void Atom::setV(double vx, double vy, double vz)
{
    m_v[0] = vx;
    m_v[1] = vy;
    m_v[2] = vz;
}

void Atom::addV(arma::vec& vec)
{
    m_v[0] += vec[0];
    m_v[1] += vec[1];
    m_v[2] += vec[2];
}

void Atom::addV(double vx, double vy, double vz)
{
    m_v[0] += vx;
    m_v[1] += vy;
    m_v[2] += vz;
}

void Atom::scaleV(double s)
{
    m_v[0] *= s;
    m_v[1] *= s;
    m_v[2] *= s;
}

std::ostream& operator<<(std::ostream& stream, Atom& atom)
{
    stream << "Atom(" << "name=" << atom.m_name << ", "
                      << "x="    << atom.m_r[0] << ", "
                      << "y="    << atom.m_r[1] << ", "
                      << "z="    << atom.m_r[2] << ", "
                      << "vx="   << atom.m_v[0] << ", "
                      << "vy="   << atom.m_v[1] << ", "
                      << "vz="   << atom.m_v[2] << ", "
                      << "m="    << atom.m_m    << ", "
                      << "q="    << atom.m_q    << ")";
    return stream;
}

void Atom::setName(std::string& name)
{
    m_name = name;
}

void Atom::setM(double m)
{
    m_m = m;
}

void Atom::setQ(double q)
{
    m_q = q;
}

std::ostream& operator<<(std::ostream& stream, std::vector<Atom>& vec)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        stream << vec[i] << endl;
    }
    return stream;
}

std::ostream& operator<<(std::ostream& stream, std::vector<Atom*>& vec)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        stream << *vec[i] << endl;
    }
    return stream;
}
