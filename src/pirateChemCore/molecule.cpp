#include "molecule.h"
#include "psystem.h"
#include "atom.h"

#include <fstream>

using namespace std;

Molecule::Molecule()
{
}

Molecule::~Molecule()
{
    for(unsigned int i = 0; i < m_atoms.size(); i++){
        if(m_atoms.at(i) != NULL){
            delete m_atoms[i];
        }
    }
}

const unsigned int Molecule::size()
{
    return m_atoms.size();
}

Atom &Molecule::newAtom(string name, double x, double y, double z,
                       double vx, double vy, double vz, double q, double m)
{
    Atom* atom = new Atom(name, x, y, z, vx, vy, vz, q, m);
    m_atoms.push_back(atom);
    return *atom;
}

void Molecule::clear()
{
    m_atoms.clear();
}


std::vector<Atom*> &Molecule::atoms()
{
    return m_atoms;
}

std::ostream& operator<<(std::ostream& stream,Molecule& molecule)
{
    return stream << "Molecule[" << molecule.m_atoms << "]"<< endl ;
}

void Molecule::setMoleculePositions(arma::mat& pos_matrix)
{
    if(pos_matrix.n_cols != size()){
        cerr << "position matrix dimensions do not match that of molecule's"<< endl;
        throw -1;
    }

    for(unsigned int i = 0; i < pos_matrix.n_cols; i ++){
        arma::vec pos_vec = pos_matrix.col(i);
        m_atoms[i]->setR(pos_vec);
    }
}

void Molecule::setMoleculeVelocities(arma::mat& vel_matrix)
{
    if(vel_matrix.n_cols != size()){
        cerr << "velocity matrix dimensions do not match that of molecule's"<< endl;
        throw -1;
    }

    for(unsigned int i = 0; i < vel_matrix.n_cols; i ++){
        arma::vec vel_vec = vel_matrix.col(i);
        m_atoms[i]->setV(vel_vec);
    }
}

void Molecule::setMoleculeMasses(arma::vec& mass_vec)
{
    if(mass_vec.n_elem != size()){
        cerr << "mass vector dimensions do not match that of molecule's"<< endl;
        throw -1;
    }

    for(unsigned int i = 0; i < mass_vec.n_elem; i ++){
        m_atoms[i]->setM(mass_vec(i));
    }
}

void Molecule::setMoleculeCharges(arma::vec& charge_vec)
{
    if(charge_vec.n_elem != size()){
        cerr << "charge vector dimensions do not match that of molecule's"<< endl;
        throw -1;
    }

    for(unsigned int i = 0; i < charge_vec.n_elem; i ++){
        m_atoms[i]->setQ(charge_vec(i));
    }
}

void Molecule::setMoleculeNames(std::vector<std::string>& names_vec)
{
    if(names_vec.size() != size()){
        cerr << "names vector dimensions do not match that of molecule's"<< endl;
        throw -1;
    }

    for(unsigned int i = 0.0; i< names_vec.size(); i++){
        m_atoms[i]->setName(names_vec[i]);
    }
}

std::ostream& operator<<(std::ostream& stream, std::vector<Molecule*>& vec)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        stream << *vec[i];
    }
    return stream;
}
