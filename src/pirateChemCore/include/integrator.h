#ifndef INTEGRATOR_H
#define INTEGRATOR_H
# include <armadillo>
# include "connectivity.h"


class System;
class LjCalc;
class SpringCalc;
class CoulombCalc;
class CoulombInterbead;

class Integrator
{
public:
    Integrator(System& system);

    //! the destructor for the class
    virtual ~Integrator();

    virtual void integrate() = 0;

    /**
     * @brief calculateForces calculates the forces acting on a system of atoms
     */
    void calculateForces();

    void forcesLj();

    void forcesCoulomb();

    void forcesSpring();

    void forcesPiezo();
    /**
     * @brief intraRpmdForces calculates the intra-bead forces within the ring polymers
     */
    void intraRpmdForces();

    /**
     * @brief interRpmdForces calculates the inter-bead forces amongst the ring polymers.
     *A mixed bead formalism is employed and hence ring polymer bead numbers must be multiples
     *of the smallest ring polymer.
     */
    void interRpmdForces();

    double getLjEnergy();

    double getCoulombEnergy();

    double getSpringEnergy();

    double getRingPolymerSpringEnergies();

    double getRingPolymerInterbeadEnergies();

    double getTotalEnergy();

protected:
    System& m_system;

//    arma::cube m_fij;

    //! matrix to hold the forces acting on the atoms in the system
    arma::mat m_fs;

    //! matrix to hold the forces acting on beads for the ring polymers
    arma::mat m_fsRing;

//    //! we wish to construct a vector of particle masses that correspond to each bead to make programming easier later
//    //! essentially we must divide our forces for these particles by the particle's mass which is not necessarily the ficticious mass
//    arma::vec m_ringPartMass;

    LjCalc* m_ljCalc;

    SpringCalc* m_springCalc;

    CoulombCalc* m_coulombCalc;

    CoulombInterbead* m_coulombInterbead;



    double m_coulombEnergy;

    double m_springEnergy;

    double m_ljEnergy;

    double m_ringPolymerSpringEnergies;

    double m_ringPolymerInterbeadEnergies;

    //! time in atomic units
    double dt;
};

#endif // INTEGRATOR_H


class VelocityVerlet: public Integrator
{
public:
    VelocityVerlet(System& system);

    void integrate();

    void updatePositions();

    void updateVelocities();


};
