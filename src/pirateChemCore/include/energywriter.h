#ifndef ENERGYWRITER_H
#define ENERGYWRITER_H
#include <iostream>
#include <fstream>
#include <string>

class GeneralMd;
class System;

class EnergyWriter
{
public:
    EnergyWriter(System& system, const std::string& filename = "");
    void writeFrame();

private:
    std::ofstream m_handle;
    System &m_system;
    int width;
    int pre;
};

#endif // ENERGYWRITER_H
