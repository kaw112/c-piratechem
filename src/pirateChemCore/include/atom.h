#ifndef ATOM_H
#define ATOM_H

#include <iostream>
#include <vector>
#include <string>
#include<armadillo>

/**
 * @brief A class to store info about atoms
 */
class Atom
{

public:
    /**
     * @brief Atom
     * @param name the atom type
     * @param x the x position
     * @param y the y position
     * @param z the z position
     * @param q the charge
     * @param m the mass
     */
    Atom(std::string name="X", double x=0.0, double y=0.0, double z=0.0,
         double vx = 0.0, double vy = 0.0, double vz = 0.0,
         double q=0.0, double m=0.0);

    ~Atom();




    //! get reference to atom name
    const std::string& name();

    /**
     * @brief setName set atom name
     * @param name atom name
     */
    void setName(std::string &name);

    //! get reference to atom mass
    const double& m();

    /**
     * @brief setM set atom mass
     * @param m mass
     */
    void setM(double m);

    //! get reference to atom charge
    const double& q();

    /**
     * @brief setQ set atom charge
     * @param q charge
     */
    void setQ(double q);

    //! get reference to atom position
    const arma::vec& r();

    /**
     * @brief setR set position using vector
     * @param vec standard vector
     */
    void setR(arma::vec &vec);

    /**
     * @brief setR set position using doubles
     * @param x the x position
     * @param y the y position
     * @param z the z position
     */
    void setR(double x, double y, double z);

    /**
     * @brief setR add to position using doubles
     * @param vec standard vector
     */
    void addR(arma::vec& vec);

    /**
     * @brief setR add to position using doubles
     * @param x the x position
     * @param y the y position
     * @param z the z position
     */
    void addR(double x, double y, double z);

    /**
     * @brief scaleR multiply position by scalar
     * @param s
     */
    void scaleR(double s);

    //! get reference to atom velocity
    const arma::vec &v();

    /**
     * @brief setV set velocity using vector
     * @param vec standard vector
     */
    void setV(arma::vec& vec);

    /**
     * @brief setV set velocity using doubles
     * @param vx the velocity in x
     * @param vy the velocity in y
     * @param vz the velocity in z
     */
    void setV(double vx, double vy, double vz);

    /**
     * @brief addV add to velocity using vector
     * @param vec standard vector
     */
    void addV(arma::vec &vec);

    /**
     * @brief addV add to velocity using doubles
     * @param vx the velocity in x
     * @param vy the velocity in y
     * @param vz the velocity in z
     */
    void addV(double vx, double vy, double vz);

    /**
     * @brief scaleV multiply velocity by scalar
     * @param s
     */
    void scaleV(double s);

    //! overload cout for atoms
    friend std::ostream& operator<<(std::ostream& stream, Atom& atom);

private:



    //! name of atom
    std::string m_name;

     //! position of atom
    arma::vec m_r;

    //! velocity of atom
    arma::vec m_v;

    //! mass of atom
    double m_m;

    //! charge of atom
    double m_q;

};

std::ostream& operator<<(std::ostream& stream, std::vector<Atom>& vec);

std::ostream& operator<<(std::ostream& stream, std::vector<Atom*>& vec);
#endif // ATOM_H
