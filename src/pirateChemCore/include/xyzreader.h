#ifndef XYZREADER_H
#define XYZREADER_H
#include "molecule.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

class XYZReader
{
public:
    XYZReader(const std::string& filename = "");

    /**
     * @brief readFrame reads in xyz file and populates molecule with atom info
     * @param molecule an instance of the molecule class; to be populated
     */
    bool readFrame(Molecule& molecule);

private:
    std::ifstream m_handle;
};

#endif // XYZREADER_H
