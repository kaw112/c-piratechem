#ifndef BOX_H
#define BOX_H

#include <vector>
#include <string>
#include <iostream>
#include<boost/utility.hpp>
#include<armadillo>

/**
 * @brief The Box class keeps track of a region of space in which the system lives
 */
class Box : private boost::noncopyable
{
public:
    /**
     * @brief Box initializes the box, and a, b, c vectors that compose it
     * @param matrix a 3x3 matrix where the sides of the box vectors are the columns
     */
    Box(arma::mat& matrix);

    //! destructor
    virtual ~Box();

    //! returns reference to m_box
    arma::mat& box();

    //! returns reference to side a
    arma::vec& a();

    //! returns reference to side b
    arma::vec& b();

    //! returns reference to side c
    arma::vec& c();

    /**
     * @brief setA resets m_a and m_box accordingly
     * @param vector
     */
    void setA(arma::vec& vector);

    /**
     * @brief setB resets m_b and m_box accordingly
     * @param vector
     */
    void setB(arma::vec& vector);

    /**
     * @brief setC resets m_c and m_box accordingly
     * @param vector
     */
    void setC(arma::vec& vector);

    /**
     * @brief fix_pbc here does nothing
     * @param vector a vector
     * @return
     */
    virtual arma::vec fix_pbc(arma::vec &vector);

    /**
     * @brief calcVolume calculates the volume of the box
     */
    void printVolume();

    /**
     * @brief dr calculates the difference position vector rj-ri
     * @param ri position ri
     * @param rj position rj
     * @return drij
     */
    virtual arma::vec dr(const arma::vec& ri, const arma::vec& rj);


protected:
    //! the matrix representing the box
    arma::mat m_box;

    //! the first column/side of the matrix/box
    arma::vec m_a;

    //! the second
    arma::vec m_b;

    //! the third
    arma::vec m_c;

    virtual void recalculate();

    double m_alength;
    double m_alength_half;
    double m_alength_inverse;
};



class BoxRectangular : public Box
{
public:
    /**
     * @brief BoxRectangular Initializes the rectangular box, and its sides, and puts matrix in diagonal form
     * @param matrix coulumns of the side vectors, in this case scaled unit vectors
     */
    BoxRectangular(arma::mat& matrix);

    /**
     * @brief makeDiagBox puts matrix in diagonal form
     */
    void makeDiagBox();

    /**
     * @brief fix_pbc takes a position vector and puts it back in the box if outside
     * @param posvec the position vector to be tested for putting in the box
     * @return returns the new position vector which should be in the box
     */
    arma::vec fix_pbc(arma::vec& posvec);

    /**
     * @brief dr calculates the difference vector rij, between two positions but for the "nearest image"
     * @param ri vector for position of ith particle
     * @param rj vector for position of jth particle
     * @return returns vector rj-ri
     */
    arma::vec dr(const arma::vec &ri,const arma::vec &rj);
private:
};

class BoxTriclinic : public Box
{
public:
    /**
     * @brief BoxTriclinic sets box and sides of box and inverse of box
     * @param matrix an arma matrix with columns that are the sides of the box
     */
    BoxTriclinic(arma::mat& matrix);

    //! returns the inverse of the box
    arma::mat inverse();

    //! returns a vector that is back inside the box if original is outside
    arma::vec fix_pbc(arma::vec &vector);

    /**
     * @brief dr calculates the difference vector rij, between two positions but for the "nearest image" (note this is only surely true if the triclinic box is near rectangular)
     * @param ri vector for position of ith particle
     * @param rj vector for position of jth particle
     * @return returns vector rj-ri
     */
    arma::vec dr(arma::vec &ri, arma::vec &rj);
private:
    arma::mat m_inverse;
};

#endif // BOX_H
