#ifndef SYSTEM_H
#define SYSTEM_H
#include "molecule.h"
#include "parameters.h"
#include "connectivity.h"
#include "atom.h"
#include "ringpolymer.h"
#include "piezo.h"
#include "timeDepField.h"
#include <armadillo>
#include <vector>
#include <string>
#include <iostream>

class Thermostat;
class Integrator;
class Box;
class GeneralMd;

class System
{
public:
    System(const std::string &inputFile = "");

   ~System();

    //! Get reference to m_molecules
    std::vector<Molecule *> &molecules();

    //!Get reference to m_atoms
    std::vector<Atom*>& atoms();

    //! Get reference to m_ringPolymers
    std::vector<RingPolymer*>& ringPolymers();

    //! Get reference to m_ringPolymerBeads
    std::vector<Atom*>& ringPolymerBeads();

    void newAtom(Molecule& molecule, std::string name="X", double x=0.0, double y=0.0, double z=0.0,
                 double vx = 0.0, double vy = 0.0, double vz = 0.0,
                 double q=0.0, double m=0.0);

    //! returns the number of molecules in the system
    const int numMol();

    //! returns the size of the system aka. # of atoms
    unsigned int size();

    //! returns the # of beads for all ring polymers
    unsigned int ringPolymerSize();

    //! returns the total size (beads + atoms) for the system
    unsigned int totalSize();

    //! get current time in simulation
    double time();

    //! returns the last molecule in the molecule list
    Molecule& lastMol();

    /**
     * @brief getBox gets a pointer to the box of the system
     * @return  pointer to box for the system
     */
    Box& getBox();

    /**
     * @brief setBox set normal box (no pbc)
     * @param matrix armadillo matrix, columns are side vectors that make up box
     */
    void setBox(arma::mat &matrix);

    /**
     * @brief setBoxTriclinic set box to triclinic box
     * @param matrix armadillo matrix, columns are side vectors that make up box
     */
    void setBoxTriclinic(arma::mat &matrix);

    /**
     * @brief setBoxRectangular
     * @param matrix
     */
    void setBoxRectangular(arma::mat &matrix);

    //! sets initial velocities of system to maxwell velocities if so desired
    void setMaxwellVelocities();

    //! makes molecule if system is told to be uniform
    void setUniformSystem();

    //! set random postions based on box;
    void setRandomPositions();

    //! set uniform positions based on box;
    void setUniformPositions();

    //! destroys initial center of mass velocity for the system
    void destroyComVel();

    //! net momentum for the system
    arma::vec netMom();

    //! get the geometric center of the system
    arma::vec getGeomCen();

    //! calculate the geometric center for the system
    void calculateGeomCen();

    //! calculate the rotation translation matrix
    void calculateRotTrans();

    //!calculate the rotational tensor
    void calculateRotTensor();

    //! get rot trans matrix
    arma::mat getRotTrans();

    //! get the rotational tensor
    arma::mat getRotTensor();

    //! get integrator for the system
    Integrator& getIntegrator();

    //! get generalmd for system;
    GeneralMd& getGeneralMd();

    //! get piezo for system
    Piezo& getPiezo();

    //! get time dep field for system
    TimeDepField& getTimeDepField();

    //! return reference to m_hessianFile
    const std::string& getHessianFile();

    //! return reference to m_dipoleDerivFile
    const std::string& getDipoleDerivFile();

    //! set piezo stuff if need be;
    void setPiezo();

    //! set time dep field; will be set along piezo
    void setTimeDepField();

    //! calculate temperature and kinetic energy for the system
    void setTempKin();

    //! get temperature for system;
    double getTemperature();

    //! get kinetic energy for system;
    double getKineticEnergy();

    //! set integrator to type determined by parameters
    void setIntegrator();

    //! set thermostat to type determined by parameters
    void setThermostat();

    //! get thermostat for the system
    Thermostat& getThermostat();

    /**
     * @brief newMolecule create new molecule via xyz
     * @param filename string for xyz file
     */
    Molecule& newMolecule(const std::string& filename);

    /**
     * @brief newMolecule create new molecule with default atom values with N atoms
     * @param N integer number of atoms to create
     */
    Molecule& newMolecule(int N);

    /**
     * @brief newMolecule create new molecule with no atoms
     */
    Molecule& newMolecule();

    /**
     * @brief newRingPolymer adds a ring polymer to m_ringPolymers
     */
    RingPolymer& newRingPolymer(unsigned int numBeads, double partMass, double fictMass, double partCharge = 0.0, double temp = 300, double cenX = 0.0, double cenY=0.0, double cenZ= 0.0);

    //! returns reference to m_systemConnectivity
    Connectivity& systemConnectivity();

    //! overload cout for system
    friend std::ostream& operator<<(std::ostream& stream, System& System);

    //! member of SimulatinParameters class to set via input file
    SimulationParameters& systemParameters();

    //! PirateChem messages
    void parley(std::string message);

    //! returns input file name
    std::string &getInputFile();

    //! set the hessian file (usually through input parser)
    void setHessianFile(const std::string& hessianFile);

    //! set dipole derivative file (usually through input parser)
    void setDipoleDerivFile(const std::string& dipDivFile);

protected:
    /**
     * @brief initialize takes care of initializing the system (ie.parsing input file, setting boxes and connectivity information, etc.)
     * @param inputFile the input file name as a string
     */
    void initialize(const std::string& inputFile = "");


    void setCoulombConnectivity();
    void setSpringConnectivity();
    void setLJConnectivity();

    //! geometric center of system
    arma::vec m_geomCen;

    //! hessian file for piezo calculations
    std::string m_hessianFile;

    //! dipole deriv file for piezo calculations
    std::string m_dipoleDerivFile;

    //! piezo object
    Piezo *m_piezo;

    //! time dep field object
    TimeDepField *m_tdField;

    //! matrix of rotation translation vectors
    arma::mat m_rotTrans;

    //! moment of inertia tensor without masses
    arma::mat m_rotTensor;

    std::string m_inputFile;

    std::vector<Molecule*> m_molecules;

    std::vector<RingPolymer*> m_ringPolymers;

    std::vector<Atom*> m_atoms;

    std::vector<Atom*> m_ringPolymerBeads;

    SimulationParameters m_systemParameters;

    Connectivity* m_systemConnectivity;

    Box *m_box;

    Integrator *m_systemIntegrator;

    Thermostat *m_systemThermostat;

    GeneralMd *m_systemGeneralMd;

    //! the temperature of the system, calculated from kinetic energy
    double m_temperature;

    //! the kinetic energy of the system
    double m_kineticEnergy;

    //! total number of atoms in molecules
    unsigned int m_size;

    //! total number of atoms in ring polymers
    unsigned int m_ringPolymerSize;
};

#endif // SYSTEM_H
