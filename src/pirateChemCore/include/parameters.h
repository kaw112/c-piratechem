#ifndef PARAMETERS_H
#define PARAMETERS_H
#include <cmath>
#include <string>
#include <iostream>
#include <boost/regex.hpp>

class SimulationParameters
{
public:
    //! The simulation temperature
    double temperature;

    //! Turn on Lennard-Jones interaction
    bool lj;

    //! Lennard-Jones cutoff interaction distance
    double lj_cutoff;

    //! turn on piezo system
    bool piezo;

    //! Turn on Coulomb interactions
    bool coulomb;

    //! Turn on Spring interactions
    bool spring;

    //! Turn on field interactions
    bool field;

    //! An external temperature for the system
    double fieldExt;

    //! Use standard connectivity vector for ljs?
    bool ljConStandard;

    //! Use standard connectivity vector for springs?
    bool springConStandard;

    //! Use standard connectivity vector for coulomb?
    bool coulombConStandard;

    //! Use to set initial velocities via the maxwell boltzmann distribution
    bool maxwellVelocities;

    //! Use to set integrator method to VelocityVerlet class
    bool velocityVerlet;

    //! Use to set noseHoover thermostat
    bool noseHoover;

    //! Use to set Langevin thermostat
    bool langevinTherm;

    //! Use to set velocity rescaling thermostat
    bool bussiTherm;

    //! Should a trajectory file be saved
    bool saveTrajectory;

    //! Should an energy and temperature file be saved
    bool saveEnergy;

    //! a parameter that gets set to true if a rectangular or triclinic box is initialized; tells us when to use pbc
    bool pbc;

    //! if true a rpmd simulation is going to be run and an rpmd section is necessary
    bool rpmd;

    //! if selected random positions inside the box will be specified for the system
    bool randomPositions;

    //! if selected atoms will be placed in uniform positions in the cell
    bool uniformPositions;

    //! if set to true the system will be made up of some uniform atom species
    bool uniformSystem;

    //! if set to true, every atom in the system will have the same mass
    double uniformMass;

    //! time step to be used for simulation
    double dt;

    //! number of steps for the simulation
    int numSteps;

    //! print on every ith step
    int printStep;

    //! output width
    int outWidth;

    //! output precision
    int outPrecision;

    //! the size of the uniform system (num atoms)
    int uniformSystemSize;

    //! output stub
    std::string outStub;

    //! name of atoms for a uniform system
    std::string uniformName;

    //! current step
    int currentStep;

    //! boltzmann factor
    double k;

    //! constant to turn angstroms into bohrs
    //double angToBohr;

    //! constant to turn bohrs into angstroms
    //double BohrToAng;

    //! constant turns amu to units of electron mass
    //double amuToMe;

    //! constant turns units of electron mass to amu
    //double meToAmu;

    //! constant turns units of femtoseconds to atomic units of time
    //double femtosecToAu;

    //! pi
    double pi;

    //! permitivity of free space
    double pfs;

    //! the tau parameter for the bussi thermostat
    double bussiTau;

    //! number of ficticious particles in NoseHooverChain
    int noseHooverM;

    //! random seed (0 means use current time)
    unsigned int randomseed;

    //! Sets default values
    SimulationParameters();

    //! Makes sure parameters are ok
    bool valid();

    //! Parse a line in an Input File's parameter section
    void lineParse(std::string &input_line);

    friend std::ostream& operator<<(std::ostream& stream, SimulationParameters& parameters);

private:
    boost::regex m_regexTrue;
    boost::regex m_regexFalse;

};

#endif // PARAMETERS_H
