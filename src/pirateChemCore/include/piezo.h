#ifndef PIEZO_H
#define PIEZO_H
#include <armadillo>
#include <boost/regex.hpp>

class System;

class Piezo
{
public:
    //!piezo class initializer
    Piezo(System& system);

    //!piezo class destructor
    ~Piezo();

    //! return reference to m_hessian
    arma::mat& getHessian();

    //! return reference to m_dipoleDeriv
    arma::mat& getDipoleDeriv();

    //! make the hessian from an already formed hessian file
    void makeHessian(const std::string& hessianFile);

    //! make the dipole derivative from an already formed dipole deriv file
    void makeDipoleDeriv(const std::string& dipoleDerivFile);

    arma::mat piezoForces();

    double piezoEnergy();

    //! calculate basis in the space of the vibrations for the system
    arma::mat calculateVibBasis();


private:

    //! create hessian from hessian file in
    void makeHessian();

    //! create dipole derivative from dipolederivative file in m_system
    void makeDipoleDerivative();

    //! obtain the positions of the atoms in m_system; must obtain initial positions first
    arma::vec obtainPositions();

    System& m_system;

    arma::mat m_hessian;

    arma::mat m_dipoleDeriv;

    //! need to know initial positions of atoms in the system because force generated depends on displacement
    arma::vec m_initialPositions;

    boost::regex m_hessRegex;

    boost::regex m_dipDerivRegex;
};


#endif // PIEZO_H
