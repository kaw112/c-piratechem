#ifndef THERMOSTAT_H
#define THERMOSTAT_H
#include <armadillo>
#include "random.h"

class System;

//class Thermostat
//{
//public:
//    Thermostat(System& system) { init() };
//    virtual ~Thermostat();
//    virtual void thermorate() {};

//protected:
//    System& m_system;
//    virtual void init() {};
//};

//class NoseHooverChain : Thermostat
//{
//public:
//    NoseHooverChain(System& system) : Thermostat() {};
//    void thermorate() { applyFusionBombs(); };

//private:
//    void initQs();
//    void applyFusionBombs();
//    void init() { initQs(); };
//};
/**
 * @brief The NoseHooverChain class
 *The thermostat class which performs nose-hoover chain thermostatting
 */
class NoseHooverChain
{
public:
    NoseHooverChain(System& system);

    //! updates the ficticious masses and velocities and real velocities for the system
    void chain();

private:

    //! ficticious positions
    arma::vec m_xs;

    //! ficticious velocities
    arma::vec m_vs;

    //! necessary for the chain
    arma::vec m_qs;

    //! necessary for the chain
    arma::vec m_gs;

    //! kT
    double m_kT;

    //! mass of ficticious particles
    double m;

    //! degrees of freedom for the system  assumed to be 3d
    int m_L;

    //! time step size for the system
    double dt;

    //! initialize the vector of qs
    void initQs();

    System& m_system;
};

class Langevin
{
public:
    Langevin(System& system);

private:
    System& m_system;
};

/**
 * @brief The Bussi class
 *the thermostat class that performs velocity rescaling thermostatting that maintains the canonical distribution
 */
class Bussi
{
public:
    Bussi(System& system);

    //! performs target kinetic energy calculation and velocity rescaling
    void letsGetBussi();

    ~Bussi();


private:

    //! time step size for the system
    double dt;

    //! parameter that determines the timescale of rescaling (infinite is instantaneous)
    double tau;

    //! target kinetic energy for the system
    double kinTarget;

    //! exp(-dt/tau)
    double expTOverTau;

    //! exp(-dt/(2tau))
    double expTOver2Tau;

    System& m_system;

    PirateChem::Normal m_rand;

    arma::vec m_randNums;
};

class Thermostat
{
public:
    Thermostat(System& system);
    ~Thermostat();

    NoseHooverChain& getNose();

    Langevin& getLangevin();

    Bussi& getBussi();

    void setThermostat();

private:
    NoseHooverChain *m_NoseHooverChain;
    Langevin *m_Langevin;
    Bussi *m_Bussi;
    System& m_system;
};


#endif // THERMOSTAT_H
