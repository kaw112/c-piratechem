#ifndef RINGPOLYMER_H
#define RINGPOLYMER_H
#include <vector>
#include <string>
#include <iostream>
#include <armadillo>
//#include "parameters.h"
#include "atom.h"
#include "random.h"


/**
 * @brief The ringpolymer class keeps track of beads for a given ringpolymer
 *(analogous to what a molecule is for atoms)
 */
class RingPolymer
{
public:
    RingPolymer(unsigned int numBeads, double partMass, double fictMass, double partCharge = 0.0, double temp = 300, double cenX = 0.0, double cenY=0.0, double cenZ= 0.0);

    //! a destructor to destroy the atoms (beads) in the ringpolymer
    ~RingPolymer();

    //! Get reference to m_beads
    std::vector<Atom *> &beads();

    //! Get number ot beads
    const unsigned int beadNumber();

    //! Get mass of particle
    double particleMass();

    //! Get ficticious mass of particle
    double fictMass();

    //! Get particle's charge
    double particleCharge();

    //! Get force constant between beads for particle
    double forceConstant();

    //! Get the energy of the ring polymer just for the spring displacements
    double calculateSpringEnergies();

    //! the columns of the returned matrix will contain the forces in atomic units acting on the beads
    void calculateForces(arma::mat &forces);

    //! Get reference to m_center (original centroid for polymer)
    arma::vec & center();

    //! calculates the force constant between beads for a given temperature and sets it for the ring polymer
    void calculateForceConstant();

    //! sets particle charge based on input (units are in charge of electron)
    void setBeadCharges();

    //! sets bead masses based on input of actual particle mass (units are amu)
    void setBeadMassesDefault();

    //! uses a boltzmann distribution to build the bead positions of the polymer
    void buildRingPolymerPositionsBoltzmann();

    /**
     * @brief NewAtom add bead to ring polymer
     * @param name element that makes up atom
     * @param x
     * @param y
     * @param z
     * @param vx
     * @param vy
     * @param vz
     * @param q
     * @param m
     */

    void newBead(std::string name="X", double x=0.0, double y=0.0, double z=0.0,
                 double vx = 0.0, double vy = 0.0, double vz = 0.0,
                 double q=0.0, double m=0.0);

//    Atom& newBead(std::string name="X", double x=0.0, double y=0.0, double z=0.0,
//                 double vx = 0.0, double vy = 0.0, double vz = 0.0,
//                 double q=0.0, double m=0.0);

    //! Destroy beads from molecule
    void clear();

    //! make a default polymer of size N
    void newPolymer(int N);

    /**
     * @brief setMoleculePositions: sets the beads' positions in space
     * @param pos_matrix: a matrix where columns of x, y, z positions for a bead
     */
    void setBeadPositions(arma::mat& pos_matrix);

    /**
     * @brief setBeadMasses
     * @param mass_vec a vector of masses where the ith mass corresponds to the ith atom
     */
    void setBeadMasses(arma::vec& mass_vec);
    //note: there is no real reason why the masses can't be different than the particle mass

    /**
     * @brief setBeadVelocities set velocities for beads of a given polymer
     * @param vel_matrix a matrix where columns are velocities of the correspondig atoms
     */
    void setBeadVelocities(arma::mat& vel_matrix);

    //! overload cout for ring polymers
    friend std::ostream& operator<<(std::ostream& stream, RingPolymer& ringPolymer);

    //! only call this once
    void initialize();

private:

    std::vector<Atom*> m_beads;

    //! the number of beads used to represent a particle
    unsigned int m_numBeads;

    //!the mass of the particle represented by the ring polymer in amu
    double m_particleMass;

    //! this is the ficticious mass of the particles.  It appears in the kinetic energy of the path integral formalism
    double m_fictMass;

    //! the charge of the particle represented by the ring polymer in units of e
    double m_particleCharge;

    //! used to determine around which point to center the ring polymer initially in angstroms
    arma::vec m_center;

    //! the force constant between beads of the polymer in atomic units
    double m_forceConstant;

    //! the temperature at which the ring polymer will do its thing
    double m_temperature;
};
    std::ostream& operator<<(std::ostream& stream, std::vector<RingPolymer>& vec);
#endif // RINGPOLYMER_H
