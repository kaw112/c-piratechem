#ifndef MOLECULE_H
#define MOLECULE_H
#include <vector>
#include <string>
#include <iostream>
#include <armadillo>

#include "atom.h"

/**
 * @brief The Molecule class  This class holds molecules; lists of atoms and info about molecules
 */
class Molecule
{
public:
    Molecule();

    //! a destructor that destroys objects that pointers point to in a class
    ~Molecule();

    //! Get reference to m_atoms
    std::vector<Atom *> &atoms();

    //! Get size of molecule
    const unsigned int size();

    /**
     * @brief NewAtom add atom to molecule
     * @param name element that makes up atom
     * @param x
     * @param y
     * @param z
     * @param vx
     * @param vy
     * @param vz
     * @param q
     * @param m
     */
    Atom& newAtom(std::string name="X", double x=0.0, double y=0.0, double z=0.0,
                 double vx = 0.0, double vy = 0.0, double vz = 0.0,
                 double q=0.0, double m=0.0);

    //! Destroy atoms from molecule
    void clear();

    //! make a default molecule of size N
    void newMolecule(int N);

    /**
     * @brief  setMoleculePositions set positions of a given molecule
     * @param pos_matrix a matrix where columns are x, y, and z positions
     */
    void setMoleculePositions(arma::mat& pos_matrix);

    /**
     * @brief setMoleculeMasses set masses of a given molecule
     * @param mass_vec a vector of masses where the ith mass corresponds to the ith atom
     */
    void setMoleculeMasses(arma::vec& mass_vec);

    /**
     * @brief setMoleculeCharges set charges of for atoms of a given molecule
     * @param charge_vec a vecetor where the ith charge corresponds to the ith atom
     */
    void setMoleculeCharges(arma::vec& charge_vec);

    /**
     * @brief setMoleculeNames set names for atoms of a given molecule
     * @param names_vec a vector of strings containing the names of the atoms
     */
    void setMoleculeNames(std::vector<std::string>& names_vec);

    /**
     * @brief setMoleculeVelocities set velocities for atoms of a given molecule
     * @param vel_matrix a matrix where columns are velocities of the correspondig atoms
     */
    void setMoleculeVelocities(arma::mat& vel_matrix);


    //! overload cout for molecules
    friend std::ostream& operator<<(std::ostream& stream, Molecule& molecule);

private:
    std::vector<Atom*> m_atoms;
};
    std::ostream& operator<<(std::ostream& stream, std::vector<Molecule*>& vec);
#endif // MOLECULE_H
