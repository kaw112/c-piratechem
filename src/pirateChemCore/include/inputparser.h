#ifndef INPUTPARSER_H
#define INPUTPARSER_H
#include <map>
#include <string>
#include <fstream>
#include <boost/regex.hpp>
#include "molecule.h"

class System;

class InputParser
{
public:
    /**
     * @brief The SectionType enum  enumerates the sections that a standard input file will contain
     */
    enum SectionType{
        MoleculeSection,
        ParametersSection,
        SpringsSection,
        LennardsSection,
        CoulombsSection,
        MassesSection,
        ChargesSection,
        VelocitiesSection,
        RecBoxSection,
        TriBoxSection,
        RpmdSection,
        PiezoSection,
        TimeDepFieldSection
    };

    /**
     * @brief InputParser initializes the section map and stores the system that is passed to the parser
     * @param system
     */
    InputParser(System& system);

    /**
     * @brief parse  parses the input file, searches for section headers
     * @param inputFile
     */
    void parse(const std::string& inputFile = "input.dat");

private:
    std::map<std::string, SectionType> m_sectionMap;
    std::fstream m_handle;
    boost::regex m_regexComments;
    boost::regex m_regexPirate;
    boost::regex m_regexSpaces;


    System& m_system;

    /**
     * @brief initializeSectionMap  maps sections to different string keywords
     */
    void initializeSectionMap();


    void parseMoleculeSection();
    void parseParameterSection();
    void parseLennardsSection();
    void parseSpringsSection();
    void parseCoulombsSection();
    void parseVelocitiesSection();
    void parseMassesSection();
    void parseChargesSection();
    void parseRectangularSection();
    void parseTriclinicSection();
    void parsePiezoSection();
    void parseTimeDepFieldSection();
    /**
     * @brief parseRpmdSection each line of the rpmd section should look like
     *"particle bead count     particle mass       particle charge     particle center x    particle center y     particle center z"
     */
    void parseRpmdSection();
};

#endif // INPUTPARSER_H
