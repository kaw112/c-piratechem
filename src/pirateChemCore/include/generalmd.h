#ifndef GENERALMD_H
#define GENERALMD_H

class System;

/**
 * @brief The generalMd class A class that is the central hub for running md simulations and stores information that constantly changes
 */
class GeneralMd
{
public:
    GeneralMd(System &system);

//    /**
//     * @brief getSystemTemperature determines the temperature of the system from the kinetic energy
//     * @return the temperature in Kelvins
//     */
//    double getSystemTemperature();

    /**
     * @brief getSystemKineticEnergy determines the kinetic energy for the system
     * @return the kinetic energy in hartree (atomic units)
     */
    double getSystemKineticEnergy();

    //! the work horse for Md calculations; progresses the system from one stem to the nex
    void iterateSystem();

    //! returns reference to system
    System& getSystem();


private:
    //!The system
    System& m_system;
};

#endif // GENERALMD_H
