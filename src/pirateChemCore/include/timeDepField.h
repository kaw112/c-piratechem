#ifndef TIMEDEPFIELD_H
#define TIMEDEPFIELD_H
#include <armadillo>
#include <boost/regex.hpp>
#include "waveForm.h"

class System;

class TimeDepField
{
public:
//! Initializer for time dep field class
TimeDepField(System& system);

//!Destructor
~TimeDepField();

//! add sine wave object to sineWaves vector
void addSineWave(double phase, double wvX, double wvY, double wvZ, double frequency);

//! calculate the total field contribution from SineWave objects at a given time
arma::vec calcTotalSineField(double time);

//! reference to m_simeWaves;
std::vector<SineWave*>& getSineWaves();

private:

System& m_system;

std::vector<SineWave*> m_sineWaves;



};

#endif // TIMEDEPFIELD_H
