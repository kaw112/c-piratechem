#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <cmath>
#include <armadillo>
#include <map>
//!hartree/K
#define KB 3.166811429e-6

//!blueberry
const double PI = atan(1) * 4.0;
const double TWO_PI = 2 * PI;


//! constant to turn angstroms into bohrs
#define angToBohr 1.889725989

//! constant to turn bohrs into angstroms
#define BohrToAng  1.0/angToBohr

//! constant turns amu to units of electron mass
#define amuToMe 1822.8884864247


//! constant turns units of electron mass to amu
#define meToAmu 1.0/amuToMe

//! constant turns units of femtoseconds to atomic units of time
#define femtosecToAu 41.341373336561

//! function to compare two floats or doubles
template <typename T> bool fuzzy_compare(T obj_1, T obj_2, T tol=1e-10) {
    return abs(obj_2 - obj_1) < tol;
}

//! compare two arma vecs even though i will never use this and do it the hard way.
inline bool operator==(const arma::vec& vec_1, const arma::vec& vec_2) {
    return fuzzy_compare(vec_1(0), vec_2(0)) &
           fuzzy_compare(vec_1(1), vec_2(1)) &
           fuzzy_compare(vec_1(2), vec_2(2));
}

static std::map<std::string, double> massMap={
  {"h", 1.00794},
  {"c", 12.0107},
  {"n", 14.00674},
  {"o", 15.9994}
};

void createMassMap();

#endif // CONSTANTS_H
