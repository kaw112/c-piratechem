#ifndef CALCMATRICES_H
#define CALCMATRICES_H
#include <armadillo>
#include <vector>
#include "connectivity.h"

class System;

/**
 * @brief The CalcMatrices class A class for organizing positions of atoms and differences of positions of atoms in matrix form (useful for calculations)
 */
class CalcMatrices
{
public:
    CalcMatrices(System& system);

    arma::mat& posVecMatrix(arma::mat& posVecMat);

    arma::mat& ljDiffMatrix(std::vector<LJ*>& conLJcutoff, arma::mat &ljDiffMat);

    arma::mat& coulomDiffMatrix(std::vector<Coulomb>& conCoulomb, arma::mat& coulombDiffMat);

    arma::mat& springDiffMatrix(std::vector<Spring>& conSpring, arma::mat &springDiffMat);

private:

    System& m_system;

};

#endif // CALCMATRICES_H
