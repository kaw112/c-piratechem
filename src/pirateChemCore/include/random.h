#ifndef RANDOM_H
#define RANDOM_H

#include<boost/shared_ptr.hpp>
#include<boost/utility.hpp>
#include<boost/random.hpp>
#include<iostream>

namespace PirateChem {

/**
 * @brief A class for using Boost random
 *
 * Do not use RandomBase explicitly.
 * RandomBase holds a common static generator.
 * By default, uses the current time to seed the generator.
 */
class RandomBase : private boost::noncopyable
{
public:
    //! type of seed
    typedef unsigned int SeedType;

    //! type of generator (mersenne twister)
    typedef boost::mt19937 GeneratorType;

    /**
     * @brief Create the random number generator
     */
    RandomBase();

    //! Destroy the random number generator
    ~RandomBase();

    //! Get the seed used
    SeedType seed();

    //! Reseed the generator
    void seed(SeedType seed);

    /**
     * @brief save the state of the random number generator to a stream
     */
    friend std::ostream& operator<<(std::ostream& stream, RandomBase& random);

    /**
     * @brief load the state of the random number generator to a stream
     */
    friend std::istream& operator>>(std::istream& stream, RandomBase& random);

protected:
    //! seed
    static SeedType m_seed;

    //! random number generator
    static GeneratorType m_generator;
};

/**
 * @brief A class to get Random doubles between 0 and 1
 */
class Random : public RandomBase
{
public:
    typedef boost::uniform_01<double> Distribution;
    typedef boost::variate_generator<GeneratorType&, Distribution> Variate;
    Random();

    //! get a random number
    double operator()();

    /**
     * @brief choose no (false) a certain percent of the time
     * @param percent number between 0 and 1
     */
    bool chooseFalse(double percent);

    /**
     * @brief choose yes (true) a certain percent of the time
     * @param percent number between 0 and 1
     */
    bool chooseTrue(double percent);

    /**
     * @brief choose yes (true) using the Boltzmann factor
     * @param deltaE energy change
     * @param beta inverse KT
     */
    bool metropolis(double deltaE, double beta);

private:
    boost::shared_ptr<Variate> m_variate;
};

/**
 * @brief A class to get random ints in an inclusive range
 */
class UniformInt : public RandomBase
{
public:
    typedef boost::uniform_int<int> Distribution;
    typedef boost::variate_generator<GeneratorType&, Distribution> Variate;

    /**
     * @brief Create generator with parameters
     * @param low lower bound
     * @param high upper bound
     */
    UniformInt(int low, int high);

    /**
     * @brief reset parameters
     * @param low lower bound
     * @param high upper bound
     */
    void setParameters(int low, int high);

    //! get a random number
    double operator()();

    /**
     * @brief reset parameters and return a random number
     * @param low lower bound
     * @param high upper bound
     */
    double operator()(int low, int high);

    /**
     * @brief randomly choose an element from a vector
     * @param vec vector of objects
     */
    template<typename T> T& choose(std::vector<T>& vec);

private:
    boost::shared_ptr<Variate> m_variate;
};

/**
 * @brief A class to get random doubles in a range
 */
class UniformReal : public RandomBase
{
public:
    typedef boost::uniform_real<double> Distribution;
    typedef boost::variate_generator<GeneratorType&, Distribution> Variate;

    /**
     * @brief Create generator with parameters
     * @param low lower bound
     * @param high upper bound
     */
    UniformReal(double low, double high);

    /**
     * @brief reset parameters
     * @param low lower bound
     * @param high upper bound
     */
    void setParameters(double low, double high);

    //! get a random number using current parameters
    double operator()();

    /**
     * @brief reset parameters and return a random number
     * @param low lower bound
     * @param high upper bound
     * @return
     */
    double operator()(double low, double high);

private:
    boost::shared_ptr<Variate> m_variate;
};

/**
 * @brief A class to get random doubles from normal distribution
 */
class Normal : public RandomBase
{
public:
    typedef boost::normal_distribution<double> Distribution;
    typedef boost::variate_generator<GeneratorType&, Distribution> Variate;

    /**
     * @brief Create generator with parameters
     * @param mu average of normal
     * @param sigma standard deviation of normal
     */
    Normal(double mu = 0, double sigma = 1);

    /**
     * @brief reset parameters
     * @param mu average of normal
     * @param sigma standard deviation of normal
     */
    void setParameters(double mu, double sigma);

    //! get a random number using current parameters
    double operator()();

    /**
     * @brief reset parameters and return a random number
     * @param mu average of normal
     * @param sigma standard deviation of normal
     */
    double operator()(double mu, double sigma);

private:
    boost::shared_ptr<Variate> m_variate;
};

template<typename T> T& UniformInt::choose(std::vector<T>& vec)
{
    int i = (*this)(0, vec.size() - 1);
    return vec[i];
}

}

#endif // RANDOM_H
