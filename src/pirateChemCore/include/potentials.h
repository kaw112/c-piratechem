#ifndef POTENTIALS_H
#define POTENTIALS_H
#include <armadillo>
#include "connectivity.h"

class System;

class LjCalc
{
public:
    LjCalc(System& system);

    /**
     * @brief forces get the lj forces acting between a pair of two particles
     * @param i integer specifying index in lj connectivity vector
     * @return vector of lj forces acting on atom2
     */
    void forces(int i, arma::vec& force);

    /**
     * @brief energy get the lj energies acting between a pair of two particles
     * @param i integer specitfying the index of the connectivity array of interest
     * @return the energy between the two particles due to lj interactions
     */
    double energy(int i);

private:
    System& m_system;
};

class SpringCalc
{
public:
    SpringCalc(System& system);

    /**
     * @brief forces get the spring forces acting between a particle pair
     * @param i the integer specifying the index of the connectivity vector
     * @return the force acting on particle atom2 due to atom i as a vector (atomic units)
     */
    void forces(int i, arma::vec& force);

    /**
     * @brief energy get the spring energy between a pair of particles connected by a spring
     * @param i the integer specifying the index of the connectivity vector
     * @return the energy between the two particles due to the stretching of their shared spring
     */
    double energy(int i);

private:
    System& m_system;
};

class CoulombCalc
{
public:
    CoulombCalc(System& system);

    /**
     * @brief forces get the coulomb forces between a pair of particles
     * @param i the integer specifying the index of the connectivity vector
     * @return the forces between the two particles as a vector due to coulomb interactions
     */
    void forces(int i, arma::vec&force);

    /**
     * @brief energy get the coulomb energy between a pair of particles
     * @param i the integer specifying the index of the connectivity vector
     * @return the energy between the two particles as a double due to coulomb interactions
     */
    double energy(int i);

private:
    System& m_system;
};

class CoulombInterbead
{
public:
    CoulombInterbead(System& system);

    void forces(int i, int j, arma::vec& force);

    double energy(int i, int j);

private:
    System& m_system;

};

#endif // POTENTIALS_H
