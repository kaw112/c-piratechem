#ifndef CONNECTIVITY_H
#define CONNECTIVITY_H
#include <vector>
#include <armadillo>

class System;

class LJ {
public:
    int atom1;
    int atom2;
    double s;
    double e;
    LJ(int index1, int index2, double sigma = 3.4, double epsilon = .00037974);
    friend bool operator==(const LJ& lj_1, const LJ& lj_2);
    friend bool operator!=(const LJ& lj_1, const LJ& lj_2);
};

class Spring{
public:
    int atom1;
    int atom2;
    double mk;
    double mr0;
    Spring(int index1, int index2, double k = .00025, double r0 =1);
    friend bool operator==(const Spring& spring_1, const Spring& spring_2);
    friend bool operator!=(const Spring& spring_1, const Spring& spring_2);
};

class Coulomb{
public:
    int atom1;
    int atom2;
    Coulomb(int index1, int index2);
    friend bool operator==(const Coulomb& coulomb_1, const Coulomb& coulomb_2);
    friend bool operator!=(const Coulomb& coulomb_1, const Coulomb& coulomb_2);
};

class Connectivity
{
public:
    Connectivity(System& system);

    /**
     * @brief addConLJ appends a member of the LJ class
     * @param lj a member of the LJ class
     */
    void addConLJ(LJ &lj);

    /**
     * @brief addConSpring appends a member of the Spring class
     * @param spring a member of the spring class
     */
    void addConSpring(Spring &spring);

    /**
     * @brief addConCoulomb appends a member of the Coulomb class
     * @param coulomb a member of the coulomb class
     */
    void addConCoulomb(Coulomb &coulomb);

    /**
     * @brief checkConLJ generates the vector of relevant LJ's after they have been tested versus a cutoff
     */
    void checkConLJ();

    //! returns reference to the LJ connectivity vector
    std::vector<LJ> &conLJ();

    //! returns reference to the Spring connectivity vector
    std::vector<Spring>& conSpring();

    //! returns reference to the Coulomb connectivity vector
    std::vector<Coulomb> &conCoulomb();

    /**
     * @brief standardConLJ generates standard connectivity vector of LJs for the system
     */
    void standardConLJ();

    /**
     * @brief standardConSpring generates standard connectivity vector of Springs for the system
     */
    void standardConSpring();

    /**
     * @brief standardConCoulomb generates standard connectivity vector of Coulombs for the system
     */
    void standardConCoulomb();

private:
    std::vector<LJ> m_conLJ;

    std::vector<Spring> m_conSpring;

    std::vector<Coulomb> m_conCoulomb;

    std::vector<LJ*> m_conLJcutoff;

    System& m_system;

};

#endif // CONNECTIVITY_H
