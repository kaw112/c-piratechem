#ifndef WAVEFORM_H
#define WAVEFORM_H
#include <armadillo>
#include <boost/regex.hpp>

class SineWave
{
public:
//! Initializer for time dep field class
SineWave(double phaseFactor, double wvX, double wvY, double wvZ, double frequency);

double getPhase();

double getFreq();

arma::vec getWaveVector();

//! calculate the field for a given time
arma::vec getField(double time);

private:

    //! phase factor for offsetting sinefunction from starting at 0
    double m_phase;

    //!
    double m_wvX;

    double m_wvY;

    double m_wvZ;

    double m_freq;
};

#endif // WAVEFORM_H
