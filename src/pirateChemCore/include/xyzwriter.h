#ifndef XYZWRITER_H
#define XYZWRITER_H
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

class System;
class Molecule;

class XYZWriter
{
public:
    XYZWriter(const std::string& filename = "");

    void writeFrame(Molecule& molecule);

    void writeFrame(System& system);

private:
    std::ofstream m_handle;
};

#endif // XYZWRITER_H
