//! @file
#include "constants.h"
#include<iostream>
//#include "atom.h"
//#include "molecule.h"
//#include "xyzreader.h"
//#include "xyzwriter.h"
//#include "random.h"
//#include "psystem.h"
//#include "box.h"
//#include "inputparser.h"
#include <armadillo>
#include <cmath>
//#include "generalmd.h"
//#include "connectivity.h"
#include "ringpolymer.h"

using namespace std;

int main(int argc, char **argv)
{
    int indexi = 0;
    for(int i= 0; i < 2; i ++){
        int indexj = indexi+10;
        for(int j = i+1; j<2; j++){
            for(int k = 0 ; k<10; k++){
                cout << indexi <<endl;
                cout << indexj <<endl;
                indexi +=1;
                indexj +=1;


            }
        }
    }
//    RingPolymer rp(10, 1.0, 0.0, 300.0, 0.0, 0.0, 0.0);

//    for (unsigned int i = 0; i < rp.beadNumber(); i++) {
//        Atom * atom = rp.beads().at(i);
//        cout << *atom << endl;
//    }
//    Atom atom("A", 1, 2, 3, 0, 0, 0, -1, 10);
//    cout << "Atom atom(\"A\", 1, 2, 3, 0, 0 , 0, -1, 10);" << endl;
//    cout << atom << endl << endl;

//    cout << "atom.setR(1, 2, 3)" << endl;
//    atom.setR(1, 2, 3);
//    cout << atom << endl << endl;

//    cout << "atom.addR(1, 1, 1);" << endl;
//    atom.addR(1, 1, 1);
//    cout << atom << endl;

//    cout << "atom.setV(1, 2, 3)" << endl;
//    atom.setV(1, 2, 3);
//    cout << atom << endl << endl;

//    cout << "atom.addV(1, 1, 1);" << endl;
//    atom.addV(1, 1, 1);
//    cout << atom << endl;

//    Molecule molecule;
//    cout << molecule.size() << endl;

//    for(int i = 0; i < 5; i++)
//    {
//        molecule.NewAtom();
//    }

//    cout << molecule;
//    cout << molecule.atoms() << endl;

//    XYZReader reader("a.xyz");
//    cout << "read frame: " << reader.readFrame(molecule) << endl;
//    cout << "molecule: " << molecule.size() << endl;

//    XYZWriter writer("adam.xyz");
//    writer.writeFrame(molecule);

//    PirateChem::Random random;
//    cout << "random number: " << random() << endl;

//    System system;
//    system.newMolecule(10);
//    cout << system << endl;
//    cout << system.size() << endl;
//    cout << system.numMol() << endl;
//    system.newMolecule("21.xyz");
//    cout << system << endl;
//    cout << system.size() << endl;
//    cout << system.numMol() << endl;


//    arma::mat matrix("1 0 0; 0 1 0; 0 0 5");
//    Box box(matrix);
//    cout << box.a() << box.b() << box.c();
//    cout << matrix << endl;
//    box.calcVolume();
//    arma::vec vector("2 0 0");
//    box.setA(vector);
//    cout << box.a() << endl;
//    cout << box.box() << endl;

//    arma::mat rmatrix("1 0 0; 0 5 0; 0 0 2");
//    BoxTriclinic rbox(rmatrix);
//    //rbox.makeDiagBox();
//    cout << rbox.box()<<endl;
//    cout << rbox.a() << endl
//         << rbox.b() << endl
//         << rbox.c() << endl;

//    arma::vec testvec("-.3 -7.3 1.5");
//    arma::vec a = rbox.fix_pbc(testvec);
//    cout << a << endl;
//    cout << testvec -vector <<endl;
//    arma::vec nearvec = rbox.dr(vector, testvec);
//    cout << nearvec << endl;

//    arma::mat x("2 0 0; 0 3 0; 0 0 4");
//    cout << x << endl;
//    arma::mat b = x.i();
//    cout << b << endl;

//    Molecule new_molecule;
//    for(int i = 0; i<3; i++){
//        new_molecule.NewAtom();
//    }
//    cout << new_molecule << endl;
//    arma::mat positions("1 0 3.3; 0.5 2 1; 2.3 1.0 3 ");
//    new_molecule.setMoleculePositions(positions);
//    cout << new_molecule <<endl;
//    arma::vec masses("1.0 .73 -.2");
//    new_molecule.setMoleculeMasses(masses);
//    cout << new_molecule <<endl;

//    arma::vec charges("1.0 .73 -.2");
//    new_molecule.setMoleculeCharges(charges);
//    cout << new_molecule <<endl;
//    vector<string> names;
//    names.push_back("x");
//    names.push_back("R");
//    names.push_back("p");
//    new_molecule.setMoleculeNames(names);
//    cout << new_molecule <<endl;

//    arma::mat vels("1 0 3.3; 0.5 2 1; 2.3 1.0 3 ");
//    new_molecule.setMoleculeVelocities(vels);
//    cout << new_molecule <<endl;

//    double N = 0;
//    for(int i = 0; i< 500; i++){


//    System system("input.dat");
//    cout << system << endl;
//    cout << system.systemParameters();
//    cout << system.systemConnectivity().conCoulomb().at(0).atom2 << endl;
//    cout << system.getBox().box() << endl;
//    cout << system.atoms()<< endl;
//    cout << system.netMom() << endl;

//    GeneralMd gen(system);
//    N+= gen.getSystemTemperature();
//    cout << gen.getSystemTemperature() << endl;

//    }
//    cout << N/500 <<endl;
//    arma::vec vector(".3 .3 .3");
//    cout << vector << endl;
//    cout << pow((arma::norm(vector, 2)),2)<< endl;


//      PirateChem::Normal random(0, 1);

//      for(int i = 0; i < 50; i++){
//          PirateChem::Normal random(0, 1);
//          double a = random.operator()();
//          cout << a <<endl;
//      }

//    arma::cube eric = arma::randu <arma::cube> (10, 10 ,3);
//    cout<<eric << endl;
//    cout << eric.subcube(arma::span(0,0),arma::span(0,0), arma::span(0,2)) <<endl;

//    arma::vec vector("0 0 0");
//    eric(0, 0, 0) = vector(0);
//    //eric.subcube(arma::span(0,0),arma::span(0,0), arma::span(0)) = vector(0);
//    cout << eric.subcube(arma::span(0,0),arma::span(0,0), arma::span(0,2)) <<endl;

//    arma::vec testvec(3);
//    cout <<testvec<<endl;
//    LJ lj_1(0, 1, 7.5, 0.01);
//    LJ lj_2(0, 1, 8.5, 0.03);
//    LJ lj_3(2, 3, 6.5, 0.02);
//    LJ lj_4(1, 0, 5.5, 0.04);

//    cout << (lj_1 == lj_2) << endl;
//    cout << (lj_1 == lj_3) << endl;
//    cout << (lj_1 == lj_4) << endl << endl;

//    cout << (lj_1 != lj_2) << endl;
//    cout << (lj_1 != lj_3) << endl;
//    cout << (lj_1 != lj_4) << endl;

//    int N = 5;
//    int M = 5;
//    arma::cube f_ij(N, M, 3);
//    f_ij.zeros();




//   for (int k = 0; k < 3; k++) {
////#pragma omp parallel for
//        for (int i = 0; i < N; i++) {
//            for (int j = 0; j < M; j++) {
//                f_ij(i, j, k) += k;
//            }
//        }
//    }



//    for (int k = 0; k < 3; k++) {
//        for (int i = 0; i < N; i++) {
//            for (int j = 0; j < M; j++) {
//                cout << f_ij(i, j, k) << ' ';
//            }
//            cout << endl;
//        }
//        cout << endl;
//    }




}
