##README FILE##

# Build Option 1 #
    1.  Open CMakeLists.txt in QtCreator
    2.  Proceed through dialogue
    3.  Press F5

# Build Option 2 #

From the command line:

``` bash
$ ls
doc  README.md  scripts  src  test
$ mkdir build
$ cd build
$ make
$ make install # optional
$ make doc # optional
```

# Documentation Notes #
    1.  Make sure there is a Doxyfile.in in same directory as CMakeLists.txt
    2.  A Doxyfile can be created using "doxygen -g" and renaming it
    3.  Set `OUTPUT_DIRECTORY = @PROJECT_SOURCE_DIR@/doc` in Doxyfile.in
    4.  Configure the Doxyfile.in parameters to your liking
    5.  Perform the steps under "Build Option 2"
    6.  `make doc` should produce the documentation
