import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import itertools
import argparse
import os
import re

def get_arguments(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='file_name', help = 'input file_name')
    parser.add_argument(dest='numOutput', help = 'number of steps at the end of trajectory to use for analysis')
    opts = parser.parse_args(args)
    return opts

def get_data(xyz_file, numOutput):
    """this is kind of misleading. the
    file format here is that of outputted xyz
    files from piratechem trajectories
    numOutput: specifies that the last so many
    steps are to be used for data
    """
    lines = []
    handle = open(xyz_file, 'r')
    line = handle.readline()
    while line:
        line = line.strip()
        if line:
            lines.append(line)
        line = handle.readline()

    regex = r'^\s*\d+\s*$'
    regex2 = r'step'

    systemSize = 0

    for line in lines:
        if (re.match(regex, line) is not None):
            systemSize = int(line)
            break

    xyz = np.zeros((systemSize * int(numOutput),3))
    vel = np.zeros((systemSize *int(numOutput),3))
    mass = np.zeros(systemSize)
    time = []

    counter = 0
    index   = 0
    for i in range(1,len(lines)+1):
        line = lines[-i]
        if (re.match(regex, line) is not None):
            counter = counter +1
            if counter == int(numOutput):
                index = len(lines) - i
                break

    newlines = lines[index:]
    #note the regular expression would be ridiculous for the
    #lines we are searching for
    # so instead we proceed as follows
    counterXyz = 0
    counterMass = 0
    counterVel = 0
    for line in newlines:

        if (re.match(regex, line) is not None):
            pass
        elif (re.match(regex2, line) is not None):
            tokens = line.strip().split()
            time.append(float(tokens[-1]))
        else:
            tokens = line.strip().split()
            xyz[counterXyz,0] = float(tokens[1])
            xyz[counterXyz,1] = float(tokens[2])
            xyz[counterXyz,2] = float(tokens[3])
            vel[counterVel,0] = float(tokens[4])
            vel[counterVel,1] = float(tokens[5])
            vel[counterVel,2] = float(tokens[6])
            if counterMass < systemSize:
                mass[counterMass] = float(tokens[7])
                counterMass = counterMass + 1
            counterVel = counterVel + 1
            counterXyz = counterXyz + 1
    time = np.array(time)
    return systemSize, xyz, vel, mass, time

def velDistribution(vel):
        """ plots a histogram of velocities in the
        x, y, and z direction for a system
        takes array of velocities as argument
        """
        binNumber = np.floor(vel.shape[0]/10.0)

        if binNumber > 100:
            binNumber = 100
        elif binNumber < 10:
            binNumber = 10


        velx = vel[:,0].flatten()
        vely = vel[:,1].flatten()
        velz = vel[:,2].flatten()

        plt.subplot(4,1, 1)
        plt.hist(velx, bins = binNumber)

        plt.subplot(4, 1, 2)
        plt.hist(vely, bins = binNumber)
        plt.subplot(4, 1, 3)
        plt.hist(velz, bins = binNumber)

        velSquared = vel*vel

        speedSquared = np.sum(velSquared, axis = 1)
        speed = np.sqrt(speedSquared)
        plt.subplot(4, 1, 4)
        plt.hist(speed, bins = binNumber)


        plt.show()
def velAutoCorrelationFunction(vel, numOutput, systemSize, time):
    """plots the velocity autocorrelation function for the
    number of steps specified and
    """
    velInitial = vel[0:systemSize,:]

    corrFuncVal = []
    for i in range(int(numOutput)):
        corValAdd = 0
        for j in range(systemSize):
            curIndex = i *systemSize + j
            corValAdd = corValAdd + np.dot(velInitial[j,:],vel[curIndex,:])
        corValAdd = corValAdd/systemSize
        corrFuncVal.append(corValAdd)

    plt.subplot(2,1,1)
    plt.plot(time,corrFuncVal)
    plt.subplot(2,1,2)

    f = np.fft.fftshift(np.fft.fft(corrFuncVal))
    dt = abs(time[1]-time[0])
    s = np.fft.fftshift(np.fft.fftfreq(time.size,dt))
    plt.plot(s, abs(f), 'r-')
    plt.xlabel('Freq (inverse femtoseconds)')
    plt.ylabel('Intensity')
    plt.xlim(np.amin(s), np.amax(s))
    plt.show()


if __name__=='__main__':
    work = os.getcwd()
    opts = get_arguments()
    sysSize, coords, vel, mass, time = get_data(opts.file_name, opts.numOutput)

    velDistribution(vel)
    velAutoCorrelationFunction(vel, opts.numOutput, sysSize, time)
