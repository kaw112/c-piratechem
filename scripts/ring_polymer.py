# -*- coding: utf-8 -*-
import numpy as np


def ring_polymer_input(filename, N, mass, charge, temp, centerx = 0, centery = 0, centerz = 0):
    """
    filename: name of file to write to; string
    N: number of beads to include in ring polymer; integer
    mass: mass of each bead me; float
    charge: charge of each beach au.; float
    temp: the temperature in K; float
    centerx,"y,"z = the x, y, and z coordinate for the center of this bad boy
    """
    handle = open(filename, 'a')


    ### write molecule section ###
    handle.write('P) molecule \n')

    for i in range(N):
        handle.write('C ' + str(centerx) + ' ' + str(centery) + ' ' + str(centerz) + '\n')

    handle.write('P) end \n\n')

    ### write masses section ###

    handle.write('P) masses\n')
    for i in range(N):
        handle.write(str(i) + ' ' + str(mass) + '\n')

    handle.write('P) end \n\n')

    ### write charges section ###

    handle.write('P) charges\n')

    for i in range(N):
         handle.write(str(i) + ' ' + str(charge) + '\n')

    handle.write('P) end \n\n')

    ### write springs section ###

    k = mass*N/(1.0/temp**2 *9.97138779e10)*3.5710643

    handle.write('P) springs\n')

    for i in range(N-1):
        handle.write(str(i) + ' ' + str(i+1) + ' ' + str(k) + '  0\n')

    handle.write(str(N-1) + ' 0' + ' ' + str(k) + ' 0\n')
    handle.write('P) end')
