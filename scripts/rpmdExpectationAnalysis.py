import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import argparse
import os
import re
from plot_xyz import get_data

def get_arguments(args = None):
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='fileName', help = 'xyz trajectory file name')
    parser.add_argument(dest='numOutput', help = 'number of steps at end of file to use for analysis')
    opts = parser.parse_args(args)
    return opts

def hydrogenAtomElectronDistance(xyzCoords,systemSize, numOutput):

    distanceSum = 0.0
    for i in range(numOutput):
        for j in range(systemSize/2):
            index = i*systemSize + j
            index1 = index + systemSize/2
            r = xyzCoords[index1, :] - xyzCoords[index, :]
            distance = np.sqrt(np.vdot(r,r))
            distanceSum = distanceSum + distance

    distanceAvg = distanceSum/(systemSize/2 * numOutput)
    return distanceAvg


if __name__=='__main__':
    work = os.getcwd()
    opts = get_arguments()
    sysSize, coords, vel, mass = get_data(opts.fileName, opts.numOutput)

    print hydrogenAtomElectronDistance(coords, sysSize,int(opts.numOutput))

