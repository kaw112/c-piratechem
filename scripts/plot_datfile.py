# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import itertools
import argparse
import os

def get_arguments(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='file_name', help='input file name')
    opts = parser.parse_args(args)
    return opts

def get_data(file_name):
    return np.genfromtxt(file_name, names=True)

def plot_column(data, column='TotalEnergy', **kwargs):
    fig, ax1 = plt.subplots(1, 1)
    x = data['Step']
    y = data[column]
    plt.plot(x, y, 'r-', **kwargs)
    plt.xlabel('Step')
    plt.ylabel(column)
    plt.xlim(np.amin(x), np.amax(x))

def plot_fft(data, column='TotalEnergy', **kwargs):
    fig, ax1 = plt.subplots(1, 1)
    x = data['Step']
    y = data[column]
    y = y - np.average(y)
    f = np.fft.fftshift(np.fft.fft(y))
    dx = abs(x[1] - x[0])
    s = np.fft.fftshift(np.fft.fftfreq(x.size, dx))
    plt.plot(s, abs(f), 'r-', **kwargs)
    plt.xlabel('Freq')
    plt.ylabel(column + ' FFT')
    plt.xlim(np.amin(s), np.amax(s))

def plot_difference(data, column1='TotalEnergy', column2='TotalEnergy', func=abs, **kwargs):
    fig, ax1 = plt.subplots(1, 1)
    x = data['Step']
    y1 = data[column1]
    y2 = data[column2]
    y = func(y2 - y1)
    plt.plot(x, y, 'r-', **kwargs)
    plt.xlabel('Step')
    plt.ylabel('|%s - %s|' % (column1, column2))
    plt.xlim(np.amin(x), np.amax(x))

if __name__ == '__main__':
    work = os.getcwd()
    opts = get_arguments()
    data = get_data(opts.file_name)

    cmap = mpl.cm.get_cmap('gist_rainbow')
    colors = itertools.cycle([cmap(i) for i in np.linspace(0, 1, 10)])

    plot_column(data, column='TotalEnergy', color=colors.next())
    plot_column(data, column='PotentialEnergy', color=colors.next())
    plot_column(data, column='KineticEnergy', color=colors.next())
    plot_column(data, column='CoulombEnergy', color=colors.next())
    plot_column(data, column='SpringEnergy', color=colors.next())
    plot_column(data, column='LJEnergy', color=colors.next())
    plot_column(data, column='Temperature', color=colors.next())
    plot_difference(data, 'KineticEnergy', 'PotentialEnergy', func=lambda x : x, color=colors.next())
    plot_fft(data, column='SpringEnergy', color=colors.next())
    plot_fft(data, column='PotentialEnergy', color=colors.next())
    plt.show()
